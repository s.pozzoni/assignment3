**ASSIGNMENT 3**

**Introduzione**

L’applicazione è stata implementata attraverso l’utilizzo di JPA, che si occupa
della gestione della persistenza dei dati di un DBMS relazionale nelle 
applicazioni che usano le piattaforme Java Platform, Standard Edition e Java 
Enterprise Edition. Per interagire con il database abbiamo utilizzato Hibernate, 
che fornendoci un servizio di Object-relational mapping (ORM) ci permette di 
garantire la persistenza dei dati sul database, attraverso la rappresentazione 
di un sistema di oggetti java.

**Istruzioni per l’importazione del progetto**

Clonare dal link della repository il progetto. Dato che il progetto utilizza 
Maven, usando un IDE come Eclipse, tutte le dipendenze  al momento 
dell’importazione vengono gestite automaticamente.  Le dipendenze, presenti 
all’interno del file pom.xml, sono: derby, junit, hibernate e mysql-connector 
per Java.  L’unico requisito minimo è avere il JDK Java 1.8 installato, poiché 
sono utilizzate spesso le Lamda Expressions. 
Il progetto non ha un main eseguibile, ma presenta il modello dell’applicazione 
all’interno della cartella src/ e i test di JUnit nella cartella Test/. 
I test mostrano l’utilizzo dell’applicazione e verificano il suo corretto 
funzionamento mentre più avanti verrà spiegata la struttura del progetto. 
Al momento della prima esecuzione di uno dei test, l’applicazione cercherà di 
connettersi al database (descritto successivamente) e se non lo trova ne verrà 
creato uno nuovo.

**Descrizione applicazione**

L’applicazione ha lo scopo di gestire un sistema di consegne a domicilio, 
che permette ai clienti di ordinare dai ristoranti presenti all’interno del 
database. I prodotti ordinati verranno consegnati dai fattorini (delivery man). 
Quest’ultimi possono prendere in carico più ordini e si distinguono in base al 
mezzo che utilizzano:
* Bike
* Motorbike
* Car

Il database è formato dalle seguenti entità:
* Product
* DeliverMan (superclasse di Rider, Motorcyclist, Driver)
* FoodOrder
* Customer
* Restaurant

Per poter eseguire un ordine, esso deve essere composto da almeno un cliente, 
un ristorante e un prodotto. Il fattorino può essere assegnato successivamente 
al momento della consegna, e dunque anche essere eventualmente rimosso senza 
eliminare l’ordine stesso. Abbiamo scelto che, anche se un ordine è formato da 
stesso cliente, ristorante e prodotto, esso verrà comunque distinto dagli altri
grazie al suo identificativo univoco idFoodOrder . Per quanto riguarda le altre 
entità, per semplicità, abbiamo deciso che la coppia nome e cognome di un 
cliente deve essere univoca e lo stesso vale per il fattorino. Per le classi 
ristorante e prodotto è invece il solo attributo nome che deve essere unico. 
Ogni fattorino ha il proprio mezzo e in base a questo viene istanziata la sua 
classe specifica: Driver, Motorcyclist o Biker; in base alla sottoclasse cambia 
invece il valore dell’attributo vehicle. 
Un prodotto può essere composto da più ingredienti, che a loro volta possono 
essere composti da altri ingredienti.  Ad ogni ingrediente è associata la 
propria quantità e lo stesso vale per quando un prodotto viene aggiunto a un 
ordine, per cui è necessario specificare anche la quantità.

**Database per la persistenza dei dati**

Per quanto riguarda il database abbiamo utilizzato Apache Derby, un relational 
database management system (RDBMS) scritto in Java. Per l’obiettivo di questo 
progetto, abbiamo scelto in particolare di usare la modalità Embedded Derby;  
per cui derby viene eseguito all’interno della JVM dell’applicazione e non è 
accessibile da altri utenti/applicazioni. Si tratta comunque di un file-based 
database, perciò la base di dati è persistente tra un’esecuzione e l’altra 
dell’applicazione. Il database si chiama deliverdb e si trova all’interno 
dell’omonima cartella.

**Modello Entità Relazione**

![ModelloENTITARELAZIONE](Document_Images/ModelloEntitàRelazione.png)

Per quanto riguarda le relazioni, abbiamo utilizzato le seguenti:
* Il cliente può effettuare più ordini, sia dallo stesso ristorante che 
da più ristoranti
* Il ristorante può prendere ordinazioni da più clienti e più volte.
* Il fattorino può consegnare più ordini a più clienti.
* Un ordine può essere composto da uno o più prodotti.
* Un prodotto può essere formato da più prodotti chiamati ingredienti.

**Modello logico**

![ModelloLOGICO](Document_Images/ModelloLogico.png)

Avendo inserito l’attributo di relazione quantità nelle relazioni 
“molti a molti” tra “prodotto e ordine” e tra “prodotto e prodotto” inizialmente
nell’implementazione non risultava sufficiente effettuare delle join table, 
abbiamo quindi dovuto creare due entità aggiuntive: OrderDetail e 
ProductComposition.
Le relazioni tra le entità sono state gestite nel seguente modo:

* Uno a molti tra Order e OrderDetail
* Uno a molti tra Product e OrderDetail
* Uno a molti tra Product e ProductComposition che imposta come chiave esterna 
principalProduct su idProduct
* Uno a molti tra Product e ProductComposition che imposta come chiave esterna 
ingredient su idProduct.

Le relazioni tra Product e ProductComposition sono in lazy mode.

**Struttura del progetto**

All’interno della repository di gitlab, il progetto è situato nella cartella 
“Assignment3Jpa”.  Qui si trovano principalmente la cartella src/ contenente 
l’applicazione, la cartella Test/ contenente per l’appunto i test di JUnit e 
infine il file “pom.xml” che include le dipendenze del progetto. 
L’applicazione, all’interno di src/ , è suddivisa nei seguenti package:
* Dao:  sta per Data Access Object e si occupa della parte di comunicazione con
il database, utilizzando l’Entity Manager per le operazioni principali di 
CRUD e di search tramite query. Sebbene BaseDao può essere utilizzata per 
qualsiasi entità , per le classi FoodOrder e Product erano necessarie anche 
delle query più specifiche e dunque sono stati creati OrderDao e ProductDao.
* Dbserver: la classe DerbyDatabase contiene i metodi per creare e collegarsi
all’embedded derby database, per disconnettersi  e infine per eliminare tutto 
il contenuto del database.
* Exceptions: presenta l’eccezione EntityWithSameNameAlreadyExistException, 
lanciata quando i criteri di unicità definiti sui nomi delle entità vengono 
violati. Ad esempio un ristorante con lo stesso nome non può essere salvato.
* Model: contiene tutte le entità del modello dell’applicazione.  In particolare
si trovano anche le due classi “OrderDetailId” e “ProductCompositionId” che 
rappresentano gli id delle tabelle delle relazioni rispettivamente tra  
FoodOrder e Prodotto e tra Prodotto e Prodotto.
* Operations: si occupano di richiamare i metodi dei Dao, nascondendo così la 
parte delle transazioni. Inoltre sanno come comportarsi in caso di eliminazione 
delle varie entità, preoccupandosi di rimuovere tutte i relativi reference di 
una certa entità. 
* Services: sono classi statiche che permettono le varie operazioni relative a 
ogni entità, senza doversi preoccupare dell’effettiva persistenza dei loro dati. 
Richiamano i metodi delle operations. 

Per quanto riguarda i Test, i package sono invece strutturati così:
* Operations: contiene i test relativi a ciascuna classe delle operations
* Services: contiene i test relativi a ciascuna classe dei services
* TestSuites: contiene le due Suite class che permettono l’esecuzione in blocco 
dei test delle operations e dei services

Con i Test scritti, si raggiunge una copertura dell’83%.

