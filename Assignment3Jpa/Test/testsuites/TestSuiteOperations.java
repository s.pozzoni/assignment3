package testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import operations.TestCustomerOperations;
import operations.TestDeliveryManOperations;
import operations.TestOrderOperations;
import operations.TestProductOperations;
import operations.TestRestaurantOperations;

@RunWith(Suite.class)
@SuiteClasses({
	TestCustomerOperations.class,
	TestDeliveryManOperations.class,
	TestOrderOperations.class,
	TestProductOperations.class,
	TestRestaurantOperations.class
})
public class TestSuiteOperations {

}
