package testsuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import services.TestCustomerServices;
import services.TestDeliveryManServices;
import services.TestOrderServices;
import services.TestProductServices;
import services.TestRestaurantServices;

@RunWith(Suite.class)
@SuiteClasses({
	TestOrderServices.class,
	TestDeliveryManServices.class,
	TestCustomerServices.class,
	TestProductServices.class,
	TestRestaurantServices.class
})


public class TestSuiteServices {

}
