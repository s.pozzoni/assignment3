package services;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.junit.AfterClass;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dbserver.DerbyDatabase;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.Customer;
import operations.CustomerOperations;

import org.junit.Before;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCustomerServices {
	
	@BeforeClass
	public static void setup() throws Exception {
		DerbyDatabase.startDerbyDatabaseServer(); 
		CustomerServices.newInstance();
	}
	
	@Before
	public void clearDatabaseData() {
		DerbyDatabase.dropDatabase(); 
	}

	@AfterClass
	public static void tearDown() {
		boolean gotSQLExc = false;
		try {
			CustomerServices.close();
			DerbyDatabase.dropDatabase();
			DerbyDatabase.stopDerbyDatabaseServer();
			fail("Derby Database didn't throw SQLException");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			//if execution reaches here
			//it indicates the derby database has shutdown
			//and thrown SQLException as expected
			gotSQLExc = true;
			assertTrue(gotSQLExc);
		}
		
	}

	
	@Test
	public void testSingleCustomer() {

		//Test Persist
		CustomerServices.createCustomer("Luciano", "Spalletti");

		//Test Read
		Customer c1 = CustomerServices.findCustomer("Luciano", "Spalletti");

		
		assertTrue(c1.getFirstName().equals("Luciano"));
		assertTrue(c1.getLastName().equals("Spalletti"));

		//Test Update
		c1=CustomerServices.updateCustomer(c1, "Antonio", "Conte");
		assertTrue(c1.getFirstName().equals("Antonio"));
		assertTrue(c1.getLastName().equals("Conte"));

		//Test Delete
		CustomerServices.deleteCustomer(c1);
		assertTrue(!CustomerServices.isPersisted(c1));
		assertTrue(CustomerServices.findAll().size() == 0);
	}


	
	@Test
	public void testMoreCustomers() {

		//Test Persist
		CustomerServices.createCustomer("Luciano", "Spalletti");
		CustomerServices.createCustomer("Ringhio", "Gattuso");
		CustomerServices.createCustomer("Max", "Mazzarri");
		
		//Test Read
		Customer c1 = CustomerServices.findCustomer("Luciano", "Spalletti");
		Customer c2 = CustomerServices.findCustomer("Ringhio", "Gattuso");
		Customer c3 = CustomerServices.findCustomer("Max", "Mazzarri");
		
		
		assertTrue(CustomerServices.findAll().size() == 3);
		assertTrue(c1.getFirstName().equals("Luciano"));
		assertTrue(c1.getLastName().equals("Spalletti"));
		
		assertTrue(c2.getFirstName().equals("Ringhio"));
		assertTrue(c2.getLastName().equals("Gattuso"));
		
		assertTrue(c3.getFirstName().equals("Max"));
		assertTrue(c3.getLastName().equals("Mazzarri"));

		//Test Update
		c3=CustomerServices.updateCustomer(c3,"Walter","Mazzarri");
		assertTrue(c3.getFirstName().equals("Walter"));
		assertTrue(c3.getLastName().equals("Mazzarri"));
		assertTrue(CustomerServices.findAll().size() == 3);

		//Test Delete
		CustomerServices.deleteCustomer(c1);
		assertTrue(CustomerServices.findAll().size() == 2);
		CustomerServices.deleteCustomer(c2);
		assertTrue(CustomerServices.findAll().size() == 1);
		CustomerServices.deleteCustomer(c3);
		assertTrue(CustomerServices.findAll().size() == 0);
	}

}
