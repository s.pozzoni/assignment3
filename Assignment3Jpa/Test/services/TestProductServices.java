package services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dbserver.DerbyDatabase;
import model.Product;

public class TestProductServices {
	
	@BeforeClass
	public static void setup() throws Exception {
		DerbyDatabase.startDerbyDatabaseServer(); 
		ProductServices.newInstance();
	}
	
	@Before
	public void clearDatabaseData() {
		DerbyDatabase.dropDatabase(); 
	}
	
	@Test
	public void testSingleProduct() {

		//Test Persist
		ProductServices.createProduct("Kebab");

		//Test Read
		Product c1 = ProductServices.findProduct("Kebab");

		assertTrue(c1.getName().equals("Kebab"));

		//Test Update
		c1=ProductServices.updateProduct(c1, "Insalata");
		assertTrue(c1.getName().equals("Insalata"));

		//Test Delete
		ProductServices.deleteProduct(c1);
		assertTrue(!ProductServices.isPersisted(c1));
		assertTrue(ProductServices.findAll().size() == 0);

	}

	@Test
	public void testMoreProducts() {

		//Test Persist		
		ProductServices.createProduct("Kebab");
		ProductServices.createProduct("Insalata");
		ProductServices.createProduct("Pomodoro");
		
		//Test Read
		Product c1 = ProductServices.findProduct("Kebab");
		Product c2 = ProductServices.findProduct("Insalata");
		Product c3 = ProductServices.findProduct("Pomodoro");
		
		assertTrue(ProductServices.findAll().size() == 3);
		assertTrue(c1.getName().equals("Kebab"));
		assertTrue(c2.getName().equals("Insalata"));
		assertTrue(c3.getName().equals("Pomodoro"));
		
		//Test Update
		c3=ProductServices.updateProduct(c3,"Cetriolo");
		assertTrue(c3.getName().equals("Cetriolo"));
		
		assertTrue(ProductServices.findAll().size() == 3);

		//Test Delete
		ProductServices.deleteProduct(c1);
		assertTrue(ProductServices.findAll().size() == 2);
		ProductServices.deleteProduct(c2);
		assertTrue(ProductServices.findAll().size() == 1);
		ProductServices.deleteProduct(c3);
		assertTrue(ProductServices.findAll().size() == 0);

	}
	
	@Test
	public void testFindBy() {
		Product p = ProductServices.createProduct("Kebab");
		Product p2 = ProductServices.createProduct("Burger");
		Product ing1 = ProductServices.createProduct("Insalata");
		Product ing2 = ProductServices.createProduct("Pomodoro");
		Product ing3 = ProductServices.createProduct("Tacchino");
		
		int q1 = 5;
		int q2 = 10;
		ProductServices.addIngredientToProduct(p, ing1, q1);
		ProductServices.addIngredientToProduct(p2, ing1, q1);
		ProductServices.addIngredientToProduct(p, ing2, q2);
		ProductServices.addIngredientToProduct(p, ing3, q2);
		
		Product p_ = ProductServices.findProduct(p.getName());
		assertTrue(p_.getName().equals(p.getName()));
		assertTrue(p_.getIngredients().size() == p.getIngredients().size());
		
		List<Product> ps = ProductServices.findIngredientsByQuantity(p, q2);
		assertTrue(ps.size() == 2);
		assertTrue(ps.get(0).getName().equals(ing3.getName()) || ps.get(1).getName().equals(ing3.getName()));
		assertTrue(ps.get(0).getName().equals(ing2.getName()) || ps.get(1).getName().equals(ing2.getName()));
		
		ps = ProductServices.findProductsByQuantity(q1);
		assertTrue(ps.size() == 2);
		assertTrue(ps.get(0).getName().equals(p.getName()) || ps.get(1).getName().equals(p2.getName()));
		assertTrue(ps.get(0).getName().equals(p.getName()) || ps.get(1).getName().equals(p2.getName()));
		
		ps = ProductServices.findProductsByIngredient(ing1.getName());
		assertTrue(ps.size() == 2);
		assertTrue(ps.get(0).getName().equals(p.getName()) || ps.get(1).getName().equals(p2.getName()));
		assertTrue(ps.get(0).getName().equals(p.getName()) || ps.get(1).getName().equals(p2.getName()));
		
		ps = ProductServices.findProductsByIngredientQuantity(ing2.getName(), q2);
		assertTrue(ps.size() == 1);
		assertTrue(ps.get(0).getName().equals(p.getName()));
	}
	
	@Test
	public void productRelationshipOneToMany() {

		//Test Persist		
		Product p1 = new Product("Kebab");
		Product ing1 = new Product("Insalata");
		ProductServices.addIngredientToProduct(p1, ing1, 1);
		
		//Test Read
		assertTrue(p1.getName().equals("Kebab"));
		assertTrue(ing1.getName().equals("Insalata"));
		assertTrue(p1.getIngredients().get(0).getIngredient().equals(ing1));
		assertTrue(p1.getIngredients().iterator().next().getIngredient().equals(ing1));


		//Test Update
		Product ing2 = ProductServices.updateProduct(ing1, "Pomodori");
		assertTrue(ing2.getName().equals("Pomodori"));
		assertTrue(p1.getIngredients().get(0).getIngredient().equals(ing2));
		assertTrue(p1.getIngredients().get(0).getIngredient().getName().equals(ing2.getName()));
		
		//Test Deletes
		ProductServices.deleteProduct(p1);
		assertNull(ProductServices.findProduct("Kebab"));
		assertNotNull(ProductServices.findProduct("Pomodori"));
		
		ProductServices.deleteProduct(ing2);
		assertNull(ProductServices.findProduct("Kebab"));
		assertNull(ProductServices.findProduct("Pomodori"));

	}
	
	@Test
	public void moreProductRelationshipOneToMany() {

		//Test Persist
		Product p1 = ProductServices.createProduct("Kebab");
		Product ing1 = ProductServices.createProduct("Insalata");
		Product ing2 = ProductServices.createProduct("Pomodoro");
		Product ing3 = ProductServices.createProduct("Pane");
		
		ProductServices.addIngredientToProduct(p1, ing1, 1);
		p1.addIngredient(ing2, 1);
		p1.addIngredient(ing3, 1);
		ProductServices.updateProduct(p1);

		//Test Read
		Product p2 = ProductServices.findProduct(p1.getName());
		Product ing4 = ProductServices.findProduct(ing1.getName());
		Product ing5 = ProductServices.findProduct(ing2.getName());
		Product ing6 = ProductServices.findProduct(ing3.getName());
		assertTrue(p1.getIdProduct() == p2.getIdProduct());
		assertTrue(p1.getName().equals(p2.getName()));
		assertTrue(ing1.getIdProduct() == ing4.getIdProduct());
		assertTrue(ing1.getName().equals(ing4.getName()));
		assertTrue(ing2.getIdProduct() == ing5.getIdProduct());
		assertTrue(ing2.getName().equals(ing5.getName()));
		assertTrue(ing3.getIdProduct() == ing6.getIdProduct());
		assertTrue(ing3.getName().equals(ing6.getName()));
		assertTrue(p2.getIngredients().get(0).getIngredient().equals(ing4));
		assertTrue(p2.getIngredients().get(1).getIngredient().equals(ing5));
		assertTrue(p2.getIngredients().get(2).getIngredient().equals(ing6));

		//Test Update
		Product ing7 = ProductServices.updateProduct(ing1, "Salsa");
		Product ing8 = ProductServices.findProduct(ing7.getName());
		assertTrue(ing1.getIdProduct() == ing8.getIdProduct());
		assertTrue(ing7.getName().equals(ing8.getName()));
		assertTrue(p2.getIngredients().get(0).getIngredient().equals(ing8));
		
		ProductServices.removeIngredientFromProduct(p2, ing8);
		assertTrue(p2.getIngredients().size() == 2);
		ProductServices.addIngredientToProduct(p2, ing8, 1);
		assertTrue(p2.getIngredients().size() == 3);


		//Test Delete
		ProductServices.deleteProduct(ing4);
		assertNull(ProductServices.findProduct(ing4.getName()));
		assertNull(ProductServices.findProduct(p2.getName()));
		
		ProductServices.deleteProduct(ing5);
		ProductServices.deleteProduct(ing6);
		assertTrue(ProductServices.findAll().size() == 0);
	}
	
	
	@AfterClass
	public static void tearDown() {
		boolean gotSQLExc = false;
		try {
			ProductServices.close();
			DerbyDatabase.dropDatabase();
			DerbyDatabase.stopDerbyDatabaseServer();
			fail("Derby Database didn't throw SQLException");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			//if execution reaches here
			//it indicates the derby database has shutdown
			//and thrown SQLException as expected
			gotSQLExc = true;
			assertTrue(gotSQLExc);
		}
		
	}
	
}
