package services;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dbserver.DerbyDatabase;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.DeliveryMan;
import model.DeliveryMan;
import model.Driver;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestDeliveryManServices {
	
	@BeforeClass
	public static void setup() throws Exception {
		DerbyDatabase.startDerbyDatabaseServer();
		DeliveryManServices.newInstance();
	}
	
	@Before
	public void clearDatabaseData() {
		DerbyDatabase.dropDatabase(); 
	}

	@AfterClass
	public static void tearDown() {
		boolean gotSQLExc = false;
		try {
			DeliveryManServices.close();
			DerbyDatabase.dropDatabase();
			DerbyDatabase.stopDerbyDatabaseServer();
			fail("Derby Database didn't throw SQLException");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			//if execution reaches here
			//it indicates the derby database has shutdown
			//and thrown SQLException as expected
			gotSQLExc = true;
			assertTrue(gotSQLExc);
		}

	}


	@Test
	public void testSingleDeliveryMan() {

		//Test Persist
		DeliveryManServices.createDeliveryMan("Luciano", "Spalletti","Car");

		//Test Read
		DeliveryMan c1 = DeliveryManServices.findDeliveryMan("Luciano", "Spalletti");


		assertTrue(c1.getFirstName().equals("Luciano"));
		assertTrue(c1.getLastName().equals("Spalletti"));

		//Test Update
		c1=DeliveryManServices.updateDeliveryMan(c1, "Antonio", "Conte");
		assertTrue(c1.getFirstName().equals("Antonio"));
		assertTrue(c1.getLastName().equals("Conte"));

		//Test Delete
		DeliveryManServices.deleteDeliveryMan(c1);
		assertTrue(!DeliveryManServices.isPersisted(c1));
		assertTrue(DeliveryManServices.findAll().size() == 0);
	}


	@Test
	public void testMoreDeliveryMans() {


		//Test Persist
		DeliveryManServices.createDeliveryMan("Luciano", "Spalletti","Car");
		DeliveryManServices.createDeliveryMan("Ringhio", "Gattuso","Car");
		DeliveryManServices.createDeliveryMan("Max", "Mazzarri","Car");

		//Test Read
		DeliveryMan c1 = DeliveryManServices.findDeliveryMan("Luciano", "Spalletti");
		DeliveryMan c2 = DeliveryManServices.findDeliveryMan("Ringhio", "Gattuso");
		DeliveryMan c3 = DeliveryManServices.findDeliveryMan("Max", "Mazzarri");

		assertTrue(DeliveryManServices.findAll().size() == 3);
		assertTrue(c1.getFirstName().equals("Luciano"));
		assertTrue(c1.getLastName().equals("Spalletti"));

		assertTrue(c2.getFirstName().equals("Ringhio"));
		assertTrue(c2.getLastName().equals("Gattuso"));

		assertTrue(c3.getFirstName().equals("Max"));
		assertTrue(c3.getLastName().equals("Mazzarri"));

		//Test Update
		c3=DeliveryManServices.updateDeliveryMan(c3,"Walter","Mazzarri");
		assertTrue(c3.getFirstName().equals("Walter"));
		assertTrue(c3.getLastName().equals("Mazzarri"));
		assertTrue(DeliveryManServices.findAll().size() == 3);

		//Test Delete
		DeliveryManServices.deleteDeliveryMan(c1);
		assertTrue(DeliveryManServices.findAll().size() == 2);
		DeliveryManServices.deleteDeliveryMan(c2);
		assertTrue(DeliveryManServices.findAll().size() == 1);
		DeliveryManServices.deleteDeliveryMan(c3);
		assertTrue(DeliveryManServices.findAll().size() == 0);


	}
}
