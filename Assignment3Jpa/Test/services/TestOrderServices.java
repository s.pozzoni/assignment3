package services;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.HashMap;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dbserver.DerbyDatabase;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.Customer;
import model.DeliveryMan;
import model.FoodOrder;
import model.Product;
import model.Restaurant;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestOrderServices {
	@BeforeClass
	public static void setup() throws Exception {
		DerbyDatabase.startDerbyDatabaseServer(); 
		CustomerServices.newInstance();
		DeliveryManServices.newInstance();;
		RestaurantServices.newInstance();
		ProductServices.newInstance();
	}
	
	@Before
	public void clearDatabaseData() {
		DerbyDatabase.dropDatabase();
		RestaurantServices.close();
		DeliveryManServices.close();
		CustomerServices.close();
		ProductServices.close();
		OrderServices.close();
	}
	
	@Test
	public void testSingleOrder() {
	
		Customer c1=CustomerServices.createCustomer("Luciano", "Spalletti");
		DeliveryMan d1= DeliveryManServices.createDeliveryMan("Antonio", "Conte", "Car");
		Restaurant r1= RestaurantServices.createRestaurant("La cava");
		Product p1 = ProductServices.createProduct("Kebab");
		FoodOrder o1 =OrderServices.addOrder(c1, r1, p1,1);
		OrderServices.assignOrderToDeliveryMan(o1, d1);
		
		FoodOrder o2 = OrderServices.findOrders(c1).get(0);
		
		assertTrue(o1.getIdOrder()==o2.getIdOrder());
		assertTrue(o1.getDeliveryMan().equals(o2.getDeliveryMan()));
		assertTrue(o1.getRestaurant().equals(o2.getRestaurant()));
		assertTrue(o1.getCustomer().equals(o2.getCustomer()));
		assertTrue(o1.getProducts().get(0).equals(o2.getProducts().get(0)));
		
		DeliveryMan d2 = DeliveryManServices.createDeliveryMan("Diego","Simeone","Car");
		Product p2 = ProductServices.createProduct("Burger");
		
		
		OrderServices.assignOrderToDeliveryMan(o1, d2);
		assertTrue(o1.getDeliveryMan().equals(d2));
		OrderServices.close();
		OrderServices.removeProductFromOrder(p1, o1);
		OrderServices.addProductToOrder(p2, 1, o1);

		o2 = OrderServices.findOrder(o1.getIdOrder());
		assertTrue(o2.getProducts().get(0).getProduct().equals(p2));
		
		OrderServices.deleteOrder(o2);
		assertNull(OrderServices.findOrder(o1.getIdOrder()));
		
		
	}
	
	//If we remove the customer, his orders will be removed automatically
	@Test
	public void testOrderDeleteFromCustomerDelete() {
		
		Customer c1=CustomerServices.createCustomer("Luciano", "Spalletti");
		DeliveryMan d1= DeliveryManServices.createDeliveryMan("Antonio", "Conte", "Car");
		Restaurant r1= RestaurantServices.createRestaurant("La cava");
		Product p1 = ProductServices.createProduct("Kebab");
		FoodOrder o1 =OrderServices.addOrder(c1, r1, p1,1);
		OrderServices.assignOrderToDeliveryMan(o1, d1);
			
		assertTrue(OrderServices.findAll().size() == 1);
		assertTrue(RestaurantServices.findAll().size()==1);
		assertTrue(DeliveryManServices.findAll().size() == 1);
		assertTrue(CustomerServices.findAll().size() == 1);
		assertTrue(ProductServices.findAll().size() == 1);
		
		CustomerServices.deleteCustomer(c1);
		
		assertTrue(OrderServices.findAll().size() == 0);
		assertTrue(RestaurantServices.findAll().size()==1);
		assertTrue(DeliveryManServices.findAll().size() == 1);
		assertTrue(CustomerServices.findAll().size() == 0);
		assertTrue(ProductServices.findAll().size() == 1);
		RestaurantServices.deleteRestaurant(r1);
		DeliveryManServices.deleteDeliveryMan(d1);
		ProductServices.deleteProduct(p1);
		
		
	}
	
	//If we remove the restaurant, his orders will be removed automatically
	@Test
	public void testOrderDeleteFromRestaurantDelete() {

		Customer c1=CustomerServices.createCustomer("Luciano", "Spalletti");
		DeliveryMan d1= DeliveryManServices.createDeliveryMan("Antonio", "Conte", "Car");
		Restaurant r1= RestaurantServices.createRestaurant("La cava");
		Product p1 = ProductServices.createProduct("Kebab");
		FoodOrder o1 = OrderServices.addOrder(c1, r1, p1,1);
		OrderServices.assignOrderToDeliveryMan(o1, d1);
			
		assertTrue(OrderServices.findAll().size() == 1);
		assertTrue(RestaurantServices.findAll().size()==1);
		assertTrue(DeliveryManServices.findAll().size() == 1);
		assertTrue(CustomerServices.findAll().size() == 1);
		assertTrue(ProductServices.findAll().size() == 1);
		
		RestaurantServices.deleteRestaurant(r1);
		
		assertTrue(OrderServices.findAll().size() == 0);
		assertTrue(RestaurantServices.findAll().size()==0);
		assertTrue(DeliveryManServices.findAll().size() == 1);
		assertTrue(CustomerServices.findAll().size() == 1);
		assertTrue(ProductServices.findAll().size() == 1);
		RestaurantServices.deleteRestaurant(r1);
		DeliveryManServices.deleteDeliveryMan(d1);
		ProductServices.deleteProduct(p1);
	}
	

	
	//If we delete the deliveryman, in all his orders the deliveryman's attribute will be set to null
	@Test
	public void testSetDeliveryManToNull() {
		Customer c1=CustomerServices.createCustomer("Luciano", "Spalletti");
		DeliveryMan d1= DeliveryManServices.createDeliveryMan("Antonio", "Conte", "Car");
		Restaurant r1= RestaurantServices.createRestaurant("La cava");
		Product p1 = ProductServices.createProduct("Kebab");
		FoodOrder o1 =OrderServices.addOrder(c1, r1, p1,1);
		OrderServices.assignOrderToDeliveryMan(o1, d1);
			
		assertTrue(OrderServices.findAll().size() == 1);
		assertTrue(RestaurantServices.findAll().size()==1);
		assertTrue(DeliveryManServices.findAll().size() == 1);
		assertTrue(CustomerServices.findAll().size() == 1);
		assertTrue(ProductServices.findAll().size() == 1);
		
		DeliveryManServices.deleteDeliveryMan(d1);
		
		assertNull(OrderServices.findOrder(o1.getIdOrder()).getDeliveryMan());
		assertTrue(OrderServices.findAll().size() == 1);
		assertTrue(RestaurantServices.findAll().size()==1);
		assertTrue(DeliveryManServices.findAll().size() == 0);
		assertTrue(CustomerServices.findAll().size() == 1);
		assertTrue(ProductServices.findAll().size() == 1);
		RestaurantServices.deleteRestaurant(r1);
		DeliveryManServices.deleteDeliveryMan(d1);
		ProductServices.deleteProduct(p1);
	}
	
	@Test
	public void testOrderDeleteFromProductDelete() {
		Customer c1=CustomerServices.createCustomer("Luciano", "Spalletti");
		DeliveryMan d1= DeliveryManServices.createDeliveryMan("Antonio", "Conte", "Car");
		Restaurant r1= RestaurantServices.createRestaurant("La cava");
		Product p1 = ProductServices.createProduct("Kebab");
		Product p2 = ProductServices.createProduct("pizza");
		HashMap<Product,Integer> p = new HashMap<Product,Integer>();
		p.put(p1, 1);
		p.put(p2, 1);

		FoodOrder o1 = OrderServices.addOrder(c1, r1, p);
		OrderServices.assignOrderToDeliveryMan(o1, d1);
		
		
		
		assertTrue(OrderServices.findAll().size() == 1);
		assertTrue(RestaurantServices.findAll().size()==1);
		assertTrue(DeliveryManServices.findAll().size() == 1);
		assertTrue(CustomerServices.findAll().size() == 1);
		assertTrue(ProductServices.findAll().size() == 2);
		
		//If I remove one product and the order has two products, then maintain the order with only one product
		ProductServices.deleteProduct(p1);
		assertTrue(OrderServices.findAll().size() == 1);
		assertTrue(RestaurantServices.findAll().size()==1);
		assertTrue(DeliveryManServices.findAll().size() == 1);
		assertTrue(CustomerServices.findAll().size() == 1);
		assertTrue(ProductServices.findAll().size() == 1);
		
		//If I remove one product and the orders have only one product, then delete also the orders
		ProductServices.deleteProduct(p2);
		assertTrue(OrderServices.findAll().size() == 0);
		assertTrue(RestaurantServices.findAll().size()==1);
		assertTrue(DeliveryManServices.findAll().size() == 1);
		assertTrue(CustomerServices.findAll().size() == 1);
		assertTrue(ProductServices.findAll().size() == 0);
		
	}

	@Test
	public void testFindBy() {
		Customer c1=CustomerServices.createCustomer("Luciano", "Spalletti");
		DeliveryMan d1= DeliveryManServices.createDeliveryMan("Antonio", "Conte", "Car");
		Restaurant r1= RestaurantServices.createRestaurant("La cava");
		Product p1 = ProductServices.createProduct("Kebab");
		FoodOrder o1 = OrderServices.addOrder(c1, r1, p1,1);
		OrderServices.assignOrderToDeliveryMan(o1, d1);
		
		Restaurant r2= RestaurantServices.createRestaurant("Forchetta d'oro");
		Product p2 = ProductServices.createProduct("pizza");
		
		FoodOrder o2 = OrderServices.addOrder(c1, r2, p2,1);
		OrderServices.assignOrderToDeliveryMan(o2, d1);
		
		assertTrue(OrderServices.findOrders(c1).size()==2);
		assertTrue(OrderServices.findOrders(d1).size()==2);
		
		assertTrue(OrderServices.findOrders(r1).size()==1);
		assertTrue(OrderServices.findOrders(r1).get(0).getIdOrder()==o1.getIdOrder());
		assertTrue(OrderServices.findOrders(r2).size()==1);
		assertTrue(OrderServices.findOrders(r2).get(0).getIdOrder()==o2.getIdOrder());
		
		assertTrue(OrderServices.findOrders(p1).size()==1);
		assertTrue(OrderServices.findOrders(p1).get(0).getIdOrder()==o1.getIdOrder());
		assertTrue(OrderServices.findOrders(p2).size()==1);
		assertTrue(OrderServices.findOrders(p2).get(0).getIdOrder()==o2.getIdOrder());
		
		assertTrue(OrderServices.findOrders(c1, r1, d1).size()==1);
		assertTrue(OrderServices.findOrders(c1, r1, d1).get(0).getIdOrder()==o1.getIdOrder());
		assertTrue(OrderServices.findOrders(c1, r2, d1).size()==1);
		assertTrue(OrderServices.findOrders(c1, r2, d1).get(0).getIdOrder()==o2.getIdOrder());
		
		assertTrue(OrderServices.findOrders(c1, r1, p1).size()==1);
		assertTrue(OrderServices.findOrders(c1, r1, p1).get(0).getIdOrder()==o1.getIdOrder());
		assertTrue(OrderServices.findOrders(c1, r2, p2).size()==1);
		assertTrue(OrderServices.findOrders(c1, r2, p2).get(0).getIdOrder()==o2.getIdOrder());
		
		assertTrue(OrderServices.findOrders(c1, r1, p2).size()==0);
		assertTrue(OrderServices.findOrders(c1, r2, p1).size()==0);
		
	}
	
	@AfterClass
	public static void tearDown() {
		boolean gotSQLExc = false;
		try {
			RestaurantServices.close();
			DeliveryManServices.close();
			CustomerServices.close();
			ProductServices.close();
			OrderServices.close();
			DerbyDatabase.dropDatabase();
			DerbyDatabase.stopDerbyDatabaseServer();
			fail("Derby Database didn't throw SQLException");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			//if execution reaches here
			//it indicates the derby database has shutdown
			//and thrown SQLException as expected
			gotSQLExc = true;
			assertTrue(gotSQLExc);
		}
	}
}
