package services;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dbserver.DerbyDatabase;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.Restaurant;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestRestaurantServices {
	
	@BeforeClass
	public static void setup() throws Exception {
		DerbyDatabase.startDerbyDatabaseServer(); 
		RestaurantServices.newInstance();
	}
	
	@Before
	public void clearDatabaseData() {
		DerbyDatabase.dropDatabase(); 
	}

	@AfterClass
	public static void tearDown() {
		boolean gotSQLExc = false;
		try {
			RestaurantServices.close();
			DerbyDatabase.dropDatabase();
			DerbyDatabase.stopDerbyDatabaseServer();
			fail("Derby Database didn't throw SQLException");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			//if execution reaches here
			//it indicates the derby database has shutdown
			//and thrown SQLException as expected
			gotSQLExc = true;
			assertTrue(gotSQLExc);
		}
		
	}

	
	@Test
	public void testSingleRestaurant() {

		//Test Persist
		RestaurantServices.createRestaurant("Grotta azzurra");

		//Test Read
		Restaurant c1 = RestaurantServices.findRestaurant("Grotta azzurra");

		assertTrue(c1.getName().equals("Grotta azzurra"));

		//Test Update
		c1=RestaurantServices.updateRestaurant(c1, "La cava");
		assertTrue(c1.getName().equals("La cava"));

		//Test Delete
		RestaurantServices.deleteRestaurant(c1);
		assertTrue(!RestaurantServices.isPersisted(c1)); 
		assertTrue(RestaurantServices.findAll().size() == 0);

	}


	
	@Test
	public void testMoreRestaurants() {


		//Test Persist
		RestaurantServices.createRestaurant("Grotta azzurra");
		RestaurantServices.createRestaurant("Fatto bene");
		RestaurantServices.createRestaurant("Pizzeria");
		
		//Test Read
		Restaurant c1 = RestaurantServices.findRestaurant("Grotta azzurra");
		Restaurant c2 = RestaurantServices.findRestaurant("Fatto bene");
		Restaurant c3 = RestaurantServices.findRestaurant("Pizzeria");
		
		
		assertTrue(RestaurantServices.findAll().size() == 3);
		assertTrue(c1.getName().equals("Grotta azzurra"));
		assertTrue(c2.getName().equals("Fatto bene"));
		assertTrue(c3.getName().equals("Pizzeria"));

		//Test Update
		c3=RestaurantServices.updateRestaurant(c3,"Forchetta d'oro");
		assertTrue(c3.getName().equals("Forchetta d'oro"));
		assertTrue(RestaurantServices.findAll().size() == 3);

		//Test Delete
		RestaurantServices.deleteRestaurant(c1);
		assertTrue(RestaurantServices.findAll().size() == 2);
		RestaurantServices.deleteRestaurant(c2);
		assertTrue(RestaurantServices.findAll().size() == 1);
		RestaurantServices.deleteRestaurant(c3);
		assertTrue(RestaurantServices.findAll().size() == 0);


	}
}
