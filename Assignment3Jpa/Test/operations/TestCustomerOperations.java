package operations;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import dbserver.DerbyDatabase;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.Customer;
import operations.CustomerOperations;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCustomerOperations {
	
	@BeforeClass
	public static void setup() throws Exception {
		DerbyDatabase.startDerbyDatabaseServer(); 
	}
	
	@Before
	public void clearDatabaseData() {
		DerbyDatabase.dropDatabase(); 
	}

	@Test
	public void testSingleCustomer() throws EntityWithSameNameAlreadyExistException {

		CustomerOperations co = new CustomerOperations();

		//Test Persist
		Customer c1 = new Customer();
		c1.setFirstName("Luciano");
		c1.setLastName("Spalletti");
		co.persist(c1);

		//Test Read
		Customer c2 = co.get(c1.getIdCustomer());
		assertTrue(c1.getIdCustomer() == c2.getIdCustomer());
		assertTrue(c1.getFirstName().equals(c2.getFirstName()));
		assertTrue(c1.getLastName().equals(c2.getLastName()));

		//Test Update
		Customer c3 = new Customer("Antonio", "Conte");
		co.update(c1, c3);
		Customer c4 = co.get(c1.getIdCustomer());
		assertTrue(c1.getIdCustomer() == c4.getIdCustomer());
		assertTrue(c3.getFirstName().equals(c4.getFirstName()));
		assertTrue(c3.getLastName().equals(c4.getLastName()));

		//Test Delete
		co.delete(c4);
		assertNull(co.get(c4.getIdCustomer()));
		assertTrue(co.getAll().size() == 0);

		co.close();
	}


	@Test
	public void testMoreCustomers() throws EntityWithSameNameAlreadyExistException {

		CustomerOperations co = new CustomerOperations();

		//Test Persist
		Customer c1 = new Customer("Luciano","Spalletti");
		Customer c2 = new Customer("Ringhio","Gattuso");
		Customer c3 = new Customer("Max","Mazzarri");
		co.persist(c1);
		co.persist(c2);
		co.persist(c3);

		//Test Read
		assertTrue(co.getAll().size() == 3);
		Customer c4 = co.get(c3.getIdCustomer());
		assertTrue(c4.getIdCustomer() == c3.getIdCustomer());
		assertTrue(c4.getFirstName().equals(c3.getFirstName()));
		assertTrue(c4.getLastName().equals(c3.getLastName()));

		//Test Update
		c3.setFirstName("Walter");
		co.update(c3); 
		c4 = co.get(c3.getIdCustomer());
		assertTrue(c4.getIdCustomer() == c3.getIdCustomer());
		assertTrue(c4.getFirstName().equals(c3.getFirstName()));
		assertTrue(c4.getLastName().equals(c3.getLastName()));

		c1.setFirstName("Antonio");
		c1.setLastName("Conte");
		co.update(c1);
		assertTrue(co.getAll().size() == 3);

		//Test Delete
		co.delete(c1);
		assertTrue(co.getAll().size() == 2);
		co.delete(c2);
		assertTrue(co.getAll().size() == 1);
		co.delete(c3);
		assertTrue(co.getAll().size() == 0);

		co.close();

	}
	
	@Test
	public void testUniqueCustomer() throws EntityWithSameNameAlreadyExistException {
		CustomerOperations co = new CustomerOperations();
		Customer c1 = new Customer("Luciano", "Spalletti");
		co.persist(c1);
		Customer c2 = new Customer("Luciano","Spalletti");
		boolean gotException = false;
		try {
			co.persist(c2);
			fail("The same customer was persisted twice. "
					+ "EntityWithSameNameAlreadyExistException wasn't thrown");
		} catch (EntityWithSameNameAlreadyExistException e) {
			gotException = true;
			assertTrue(gotException);
		} finally {
			co.close();
		}
	}
	
	@AfterClass
	public static void tearDown() {
		boolean gotSQLExc = false;
		try {
			DerbyDatabase.dropDatabase();
			DerbyDatabase.stopDerbyDatabaseServer();
			fail("Derby Database didn't throw SQLException");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			//if execution reaches here
			//it indicates the derby database has shutdown
			//and thrown SQLException as expected
			gotSQLExc = true;
			assertTrue(gotSQLExc);
		}
	}
}
