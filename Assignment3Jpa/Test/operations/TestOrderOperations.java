package operations;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.Iterator;

import org.hibernate.Hibernate;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dao.BaseDao;
import dbserver.DerbyDatabase;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.Customer;
import model.DeliveryMan;
import model.Driver;
import model.FoodOrder;
import model.OrderDetail;
import model.Product;
import model.Restaurant;
import operations.CustomerOperations;
import operations.DeliveryManOperations;
import operations.OrderOperations;
import operations.ProductOperations;
import operations.RestaurantOperations;

public class TestOrderOperations {
	
	@BeforeClass
	public static void setup() throws Exception {
		DerbyDatabase.startDerbyDatabaseServer(); 
	}
	
	@Before
	public void clearDatabaseData() {
		DerbyDatabase.dropDatabase(); 
	}
	
	@Test
	public void testSingleOrder() throws EntityWithSameNameAlreadyExistException {
		OrderOperations oo = new OrderOperations();
		CustomerOperations co = new CustomerOperations();
		RestaurantOperations ro = new RestaurantOperations();
		DeliveryManOperations dop = new DeliveryManOperations();
		ProductOperations po =new ProductOperations();
		
		FoodOrder o1 = new FoodOrder();
		Customer c1 = new Customer("Luciano","Spalletti");
		DeliveryMan d1 = new Driver("Antonio","Conte");
		Restaurant r1 = new Restaurant("La cava");
		Product p1 = new Product("Kebab");
		o1.setCustomer(c1);
		o1.setDeliveryMan(d1);
		o1.setRestaurant(r1);
		o1.addProduct(p1, 1);
		
		//Test Persist
		po.persist(p1);
		co.persist(c1);
		dop.persist(d1);
		ro.persist(r1);
		oo.persist(o1);
		
		//Test Read
		FoodOrder o2 = oo.get(o1.getIdOrder());
		Product p2 = po.get(p1.getIdProduct());
		
		assertTrue(o1.getIdOrder() == o2.getIdOrder());
		assertTrue(o1.getRestaurant().getIdRestaurant() == o2.getRestaurant().getIdRestaurant());
		assertTrue(o1.getRestaurant().getName().equals(o1.getRestaurant().getName()));

		assertTrue(o1.getCustomer().getIdCustomer() == o2.getCustomer().getIdCustomer());
		assertTrue(o1.getCustomer().getFirstName().equals(o2.getCustomer().getFirstName()));
		assertTrue(o1.getCustomer().getLastName().equals(o2.getCustomer().getLastName()));

		assertTrue(o1.getDeliveryMan().getIdDeliveryMan() == o2.getDeliveryMan().getIdDeliveryMan());
		assertTrue(o1.getDeliveryMan().getFirstName().equals(o2.getDeliveryMan().getFirstName()));
		assertTrue(o1.getDeliveryMan().getLastName().equals(o2.getDeliveryMan().getLastName()));
		
		OrderDetail od = o2.getProducts().iterator().next();
		assertTrue(od.getProduct().getIdProduct()==p2.getIdProduct());
		assertTrue(od.getProduct().getName().equals(p2.getName()));
		assertTrue(od.getQuantity() == 1);
		
		//Test Update
		Customer c2 = new Customer("Max","Allegri");
		Restaurant r2 = new Restaurant("La Grotta Azzurra");
		DeliveryMan d2 = new Driver("Diego","Simeone");
		Product p3 = new Product("Burger");
		co.persist(c2);
		ro.persist(r2);
		dop.persist(d2);
		po.persist(p3);
		o2.setCustomer(c2);
		o2.setRestaurant(r2);
		o2.setDeliveryMan(d2);
		o2.removeProduct(p2);
		o2.addProduct(p3, 2);
		oo.update(o2);
		FoodOrder o3 = oo.get(o2.getIdOrder());
		
		assertTrue(o3.getCustomer().getFirstName().equals(c2.getFirstName()));
		assertTrue(o3.getCustomer().getLastName().equals(c2.getLastName()));
		
		assertTrue(o3.getRestaurant().getName().equals(r2.getName()));
		
		assertTrue(o3.getDeliveryMan().getFirstName().equals(d2.getFirstName()));
		assertTrue(o3.getDeliveryMan().getLastName().equals(d2.getLastName()));
		
		assertTrue(o3.getProducts().size() == 1);
		OrderDetail od2 = o3.getProducts().iterator().next();
		assertTrue(od2.getProduct().getName().equals(p3.getName()));
		assertTrue(od2.getQuantity() == 2);
		
		//Test Delete
		oo.delete(o3);
		assertTrue(oo.getAll().size() == 0);
		assertTrue(ro.getAll().size() == 2);
		assertTrue(co.getAll().size() == 2);
		assertTrue(dop.getAll().size() == 2);
		assertTrue(po.getAll().size() == 2);
		
		co.delete(c1);
		co.delete(c2);
		ro.delete(r1);
		ro.delete(r2);
		dop.delete(d1);
		dop.delete(d2);
		po.delete(p2);
		po.delete(p3);

		assertTrue(oo.getAll().size() == 0);
		assertTrue(ro.getAll().size() == 0);
		assertTrue(co.getAll().size() == 0);
		assertTrue(dop.getAll().size() == 0);
		assertTrue(po.getAll().size() == 0);
		
		oo.close();
		co.close();
		ro.close();
		dop.close();
		po.close();
	}
	
	//If we remove the customer, his orders will be removed automatically
	@Test
	public void testOrderDeleteFromCustomerDelete() throws EntityWithSameNameAlreadyExistException {
		OrderOperations oo = new OrderOperations();
		CustomerOperations co = new CustomerOperations();
		RestaurantOperations ro = new RestaurantOperations();
		DeliveryManOperations dop = new DeliveryManOperations();
		ProductOperations po = new ProductOperations();
		
		FoodOrder o1 = new FoodOrder();
		Customer c1 = new Customer("Luciano","Spalletti");
		DeliveryMan d1 = new Driver("Antonio","Conte");
		Restaurant r1 = new Restaurant("La cava");
		Product p1 = new Product("Kebab");
		o1.setCustomer(c1);
		o1.setDeliveryMan(d1);
		o1.setRestaurant(r1);
		o1.addProduct(p1, 1);
		
		po.persist(p1);
		co.persist(c1);
		dop.persist(d1);
		ro.persist(r1);
		oo.persist(o1);
		
		assertTrue(oo.getAll().size() == 1);
		assertTrue(ro.getAll().size() == 1);
		assertTrue(dop.getAll().size() == 1);
		assertTrue(co.getAll().size() == 1);
		assertTrue(po.getAll().size() == 1);
		
		co.delete(c1);	
		
		assertTrue(oo.getAll().size() == 0);
		assertTrue(ro.getAll().size() == 1);
		assertTrue(dop.getAll().size() == 1);
		assertTrue(co.getAll().size() == 0);
		assertTrue(po.getAll().size() == 1);
		ro.delete(r1);
		dop.delete(d1);
		po.delete(p1);
		
		oo.close();
		co.close();
		ro.close();
		dop.close();
		po.close();
	}
	
	//If we remove the restaurant, his orders will be removed automatically
	@Test
	public void testOrderDeleteFromRestaurantDelete() throws EntityWithSameNameAlreadyExistException {
		OrderOperations oo = new OrderOperations();
		CustomerOperations co = new CustomerOperations();
		RestaurantOperations ro = new RestaurantOperations();
		DeliveryManOperations dop = new DeliveryManOperations();
		ProductOperations po = new ProductOperations();
		
		FoodOrder o1 = new FoodOrder();
		Customer c1 = new Customer("Luciano","Spalletti");
		DeliveryMan d1 = new Driver("Antonio","Conte");
		Restaurant r1 = new Restaurant("La cava");
		Product p1 = new Product("Kebab");
		o1.setCustomer(c1);
		o1.setDeliveryMan(d1);
		o1.setRestaurant(r1);
		o1.addProduct(p1, 1);
		
		po.persist(p1);
		co.persist(c1);
		dop.persist(d1);
		ro.persist(r1);
		oo.persist(o1);
		
		assertTrue(oo.getAll().size() == 1);
		assertTrue(ro.getAll().size() == 1);
		assertTrue(dop.getAll().size() == 1);
		assertTrue(co.getAll().size() == 1);
		assertTrue(po.getAll().size() == 1);
		
		ro.delete(r1);	
		
		assertTrue(oo.getAll().size() == 0);
		assertTrue(ro.getAll().size() == 0);
		assertTrue(dop.getAll().size() == 1);
		assertTrue(co.getAll().size() == 1);
		assertTrue(po.getAll().size() == 1);
		co.delete(c1);
		dop.delete(d1);
		po.delete(p1);
		
		oo.close();
		co.close();
		ro.close();
		dop.close();
		po.close();
	}
	

	
	//If we delete the deliveryman, in all his orders the deliveryman's attribute will be set to null
	@Test
	public void testSetDeliveryManToNull() throws EntityWithSameNameAlreadyExistException {
		OrderOperations oo = new OrderOperations();
		CustomerOperations co = new CustomerOperations();
		RestaurantOperations ro = new RestaurantOperations();
		DeliveryManOperations dop = new DeliveryManOperations();
		ProductOperations po = new ProductOperations();
		FoodOrder o1 = new FoodOrder();
		Customer c1 = new Customer("Luciano","Spalletti");
		DeliveryMan d1 = new Driver("Antonio","Conte");
		Restaurant r1 = new Restaurant("La cava");
		Product p1 = new Product("Kebab");
		o1.setCustomer(c1);
		o1.setDeliveryMan(d1);
		o1.setRestaurant(r1);
		o1.addProduct(p1, 1);
		
		po.persist(p1);
		co.persist(c1);
		dop.persist(d1);
		ro.persist(r1);
		oo.persist(o1);
		
		assertTrue(oo.getAll().size() == 1);
		dop.delete(d1);
		assertTrue(oo.getAll().size() == 1);
		assertTrue(dop.getAll().size() == 0);
		assertTrue(ro.getAll().size() == 1);
		assertTrue(co.getAll().size() == 1);
		assertTrue(po.getAll().size() == 1);
		
		oo.delete(o1);
		co.delete(c1);
		ro.delete(r1);
		po.delete(p1);
		
		oo.close();
		co.close();
		ro.close();
		dop.close();
		po.close();
	}
	
	@Test
	public void testOrderDeleteFromProductDelete() throws EntityWithSameNameAlreadyExistException {
		OrderOperations oo = new OrderOperations();
		ProductOperations po = new ProductOperations();
		
		FoodOrder o1 = new FoodOrder();
		FoodOrder o2 = new FoodOrder();
		Product p1 = new Product("Kebab");
		Product p2 = new Product("Pizza");
		
		po.persist(p1);
		po.persist(p2);
		o1.addProduct(p1, 5);
		o1.addProduct(p2, 10);
		oo.persist(o1);	
		o2.addProduct(p1, 2);
		oo.persist(o2);

		assertTrue(oo.getAll().size() == 2);
		assertTrue(oo.get(o1.getIdOrder()).getProducts().size() == 2);
		assertTrue(oo.get(o2.getIdOrder()).getProducts().size() == 1);
		assertTrue(po.getAll().size() == 2);
		assertTrue(po.get(p1.getIdProduct()).getOrders().size() == 2);
		assertTrue(po.get(p2.getIdProduct()).getOrders().size() == 1);
		
		//If I remove one product and the order has two products, then maintain the order with only one product
		po.delete(p2);
		assertTrue(oo.getAll().size() == 2);
		assertTrue(oo.get(o1.getIdOrder()).getProducts().size() == 1);
		assertTrue(oo.get(o2.getIdOrder()).getProducts().size() == 1);
		assertTrue(po.getAll().size() == 1);
		
		//If I remove one product and the orders have only one product, then delete also the orders
		po.delete(p1);
		assertTrue(oo.getAll().size() == 0);
		assertTrue(po.getAll().size() == 0);
		
		oo.close();
		po.close();
	}
	
	@Test
	public void moreOrders() throws EntityWithSameNameAlreadyExistException {
		OrderOperations oo = new OrderOperations();
		CustomerOperations co = new CustomerOperations();
		RestaurantOperations ro = new RestaurantOperations();
		DeliveryManOperations dop = new DeliveryManOperations();
		ProductOperations po =new ProductOperations();
		
		//Test Persist
		FoodOrder o1 = new FoodOrder();
		Customer c1 = new Customer("Luciano","Spalletti");
		DeliveryMan d1 = new Driver("Antonio","Conte");
		Restaurant r1 = new Restaurant("La cava");
		Product p1 = new Product("Kebab");
		Product p2 = new Product("Pizza margherita");
		
		o1.setCustomer(c1);
		o1.setDeliveryMan(d1);
		o1.setRestaurant(r1);
		o1.addProduct(p1, 1);
		o1.addProduct(p2, 1);
		
		po.persist(p1);
		po.persist(p2);
		co.persist(c1);
		dop.persist(d1);
		ro.persist(r1);
		oo.persist(o1);

		//Test Read
		FoodOrder o2 = oo.get(o1.getIdOrder());
		Product p3 = po.get(p1.getIdProduct());
		Product p4 = po.get(p2.getIdProduct());
		
		assertTrue(o1.getIdOrder() == o2.getIdOrder());
		assertTrue(o1.getRestaurant().getIdRestaurant() == o2.getRestaurant().getIdRestaurant());
		assertTrue(o1.getRestaurant().getName().equals(o2.getRestaurant().getName()));
		assertTrue(o1.getCustomer().getIdCustomer() == o2.getCustomer().getIdCustomer());
		assertTrue(o1.getCustomer().getFirstName().equals(o2.getCustomer().getFirstName()));
		assertTrue(o1.getCustomer().getLastName().equals(o2.getCustomer().getLastName()));
		assertTrue(o1.getDeliveryMan().getIdDeliveryMan() == o2.getDeliveryMan().getIdDeliveryMan());
		assertTrue(o1.getDeliveryMan().getFirstName().equals(o2.getDeliveryMan().getFirstName()));
		assertTrue(o1.getDeliveryMan().getLastName().equals(o2.getDeliveryMan().getLastName()));
		
		Iterator<OrderDetail> it = o1.getProducts().iterator();
		Product pt = it.next().getProduct();
		assertTrue(pt.getIdProduct()==p3.getIdProduct());
		assertTrue(pt.getName().equals(p3.getName()));
		pt=it.next().getProduct();
		assertTrue(pt.getIdProduct()==p4.getIdProduct());
		assertTrue(pt.getName().equals(p4.getName()));
		
		//Insert new order
		FoodOrder or1 = new FoodOrder();
		
		or1.setCustomer(c1);
		or1.setDeliveryMan(d1);
		or1.setRestaurant(r1);
		or1.addProduct(p1, 1);
		or1.addProduct(p2, 1);
		
		oo.persist(or1);
		FoodOrder or2 = oo.get(or1.getIdOrder());
		
		assertTrue(or1.getIdOrder() == or2.getIdOrder());
		assertTrue(or1.getRestaurant().getIdRestaurant() == or2.getRestaurant().getIdRestaurant());
		assertTrue(or1.getRestaurant().getName().equals(or2.getRestaurant().getName()));
		assertTrue(or1.getCustomer().getIdCustomer() == or2.getCustomer().getIdCustomer());
		assertTrue(or1.getCustomer().getFirstName().equals(or2.getCustomer().getFirstName()));
		assertTrue(or1.getCustomer().getLastName().equals(or2.getCustomer().getLastName()));
		assertTrue(o1.getDeliveryMan().getIdDeliveryMan() == o2.getDeliveryMan().getIdDeliveryMan());
		assertTrue(o1.getDeliveryMan().getFirstName().equals(o2.getDeliveryMan().getFirstName()));
		assertTrue(o1.getDeliveryMan().getLastName().equals(o2.getDeliveryMan().getLastName()));
		
		Iterator<OrderDetail> it2 = or1.getProducts().iterator();
		pt = it2.next().getProduct();
		assertTrue(pt.getIdProduct()==p3.getIdProduct());
		assertTrue(pt.getName().equals(p3.getName()));
		pt=it2.next().getProduct();
		assertTrue(pt.getIdProduct()==p4.getIdProduct());
		assertTrue(pt.getName().equals(p4.getName()));
		
		
		//Test Update
		DeliveryMan d3 = new Driver("Diego","Simeone");
		dop.persist(d3);
		o1.setDeliveryMan(d3);
		or1.setDeliveryMan(d3);
		oo.update(o1);
		oo.update(or1);
		FoodOrder o4 = oo.get(o1.getIdOrder());
		FoodOrder or4 = oo.get(or1.getIdOrder());

		assertTrue(o1.getIdOrder() == o4.getIdOrder());

		assertTrue(o1.getRestaurant().getIdRestaurant() == o4.getRestaurant().getIdRestaurant());
		assertTrue(o1.getRestaurant().getName().equals(o1.getRestaurant().getName()));

		assertTrue(o1.getCustomer().getIdCustomer() == o4.getCustomer().getIdCustomer());
		assertTrue(o1.getCustomer().getFirstName().equals(o4.getCustomer().getFirstName()));
		assertTrue(o1.getCustomer().getLastName().equals(o4.getCustomer().getLastName()));

		assertTrue(d3.getIdDeliveryMan() == o4.getDeliveryMan().getIdDeliveryMan());
		assertTrue(d3.getFirstName().equals(o4.getDeliveryMan().getFirstName()));
		assertTrue(d3.getLastName().equals(o4.getDeliveryMan().getLastName()));
		
		assertTrue(or1.getIdOrder() == or4.getIdOrder());

		assertTrue(or1.getRestaurant().getIdRestaurant() == or4.getRestaurant().getIdRestaurant());
		assertTrue(or1.getRestaurant().getName().equals(or1.getRestaurant().getName()));

		assertTrue(or1.getCustomer().getIdCustomer() == or4.getCustomer().getIdCustomer());
		assertTrue(or1.getCustomer().getFirstName().equals(or4.getCustomer().getFirstName()));
		assertTrue(or1.getCustomer().getLastName().equals(or4.getCustomer().getLastName()));

		assertTrue(d3.getIdDeliveryMan() == or4.getDeliveryMan().getIdDeliveryMan());
		assertTrue(d3.getFirstName().equals(or4.getDeliveryMan().getFirstName()));
		assertTrue(d3.getLastName().equals(or4.getDeliveryMan().getLastName()));

		
		//Test Delete
		oo.delete(o1);
		oo.delete(or1);
		ro.delete(r1);
		dop.delete(d1);
		dop.delete(d3);
		co.delete(c1);
		po.delete(p1);
		po.delete(p2);
		assertTrue(oo.getAll().size() == 0);
		assertTrue(ro.getAll().size() == 0);
		assertTrue(dop.getAll().size() == 0);
		assertTrue(co.getAll().size() == 0);
		assertTrue(po.getAll().size() == 0);
		
		oo.close();
		co.close();
		ro.close();
		dop.close();
		po.close();

	}
	
	@AfterClass
	public static void tearDown() {
		boolean gotSQLExc = false;
		try {
			DerbyDatabase.dropDatabase();
			DerbyDatabase.stopDerbyDatabaseServer();
			fail("Derby Database didn't throw SQLException");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			//if execution reaches here
			//it indicates the derby database has shutdown
			//and thrown SQLException as expected
			gotSQLExc = true;
			assertTrue(gotSQLExc);
		}
	}
	
}
