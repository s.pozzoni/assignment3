package operations;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dbserver.DerbyDatabase;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.Customer;
import model.Restaurant;

public class TestRestaurantOperations {
	
	@BeforeClass
	public static void setup() throws Exception {
		DerbyDatabase.startDerbyDatabaseServer(); 
	}
	
	@Before
	public void clearDatabaseData() {
		DerbyDatabase.dropDatabase(); 
	}
	
	@Test
	public void testSingleRestaurant() throws EntityWithSameNameAlreadyExistException {
		RestaurantOperations ro = new RestaurantOperations();

		//Test Persist
		Restaurant r1 = new Restaurant();
		r1.setName("Grotta Azzurra");
		ro.persist(r1);

		//Test Read
		Restaurant r2 = ro.get(r1.getIdRestaurant());
		assertTrue(r1.getIdRestaurant() == r2.getIdRestaurant());
		assertTrue(r1.getName().equals(r2.getName()));

		//Test Update
		Restaurant r3 = new Restaurant("La cava");
		ro.update(r1, r3);
		Restaurant r4 = ro.get(r1.getIdRestaurant());
		assertTrue(r1.getIdRestaurant() == r4.getIdRestaurant());
		assertTrue(r3.getName().equals(r4.getName()));
		assertTrue(r3.getName().equals(r4.getName()));

		//Test Delete
		ro.delete(r4);
		assertNull(ro.get(r4.getIdRestaurant()));
		assertTrue(ro.getAll().size() == 0);

		ro.close();
	}

	@Test
	public void testMoreRestaurants() throws EntityWithSameNameAlreadyExistException {
		RestaurantOperations ro = new RestaurantOperations();

		//Test Persist
		Restaurant r1 = new Restaurant("Grotta Azzurra");
		Restaurant r2 =new Restaurant("La cava");
		Restaurant r3 = new Restaurant("Il Granero");
		ro.persist(r1);
		ro.persist(r2);
		ro.persist(r3);

		//Test Read
		assertTrue(ro.getAll().size() == 3);
		Restaurant r4 = ro.get(r3.getIdRestaurant());
		assertTrue(r4.getIdRestaurant() == r3.getIdRestaurant());
		assertTrue(r4.getName().equals(r3.getName()));;

		//Test Update
		r3.setName("Il mulino");
		ro.update(r3);
		r4 = ro.get(r3.getIdRestaurant());
		assertTrue(r4.getIdRestaurant() == r3.getIdRestaurant());
		assertTrue(r4.getName().equals(r3.getName()));

		r1.setName("Hangar Burger");
		ro.update(r1);
		assertTrue(ro.getAll().size() == 3);

		//Test Delete
		ro.delete(r1);
		assertTrue(ro.getAll().size() == 2);
		ro.delete(r2);
		assertTrue(ro.getAll().size() == 1);
		ro.delete(r3);
		assertTrue(ro.getAll().size() == 0);

		ro.close();
	}
	
	@Test
	public void testUniqueRestaurant() throws EntityWithSameNameAlreadyExistException {
		RestaurantOperations ro = new RestaurantOperations();
		Restaurant r1 = new Restaurant("La cava");
		ro.persist(r1);
		Restaurant r2 = new Restaurant("La cava");
		boolean gotException = false;
		try {
			ro.persist(r2);
			fail("The same restaurant was persisted twice. "
					+ "EntityWithSameNameAlreadyExistException wasn't thrown");
		} catch (EntityWithSameNameAlreadyExistException e) {
			gotException = true;
			assertTrue(gotException);
		} finally {
			ro.close();
		}
	}
	
	
	@AfterClass
	public static void tearDown() {
		boolean gotSQLExc = false;
		try {
			DerbyDatabase.dropDatabase();
			DerbyDatabase.stopDerbyDatabaseServer();
			fail("Derby Database didn't throw SQLException");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			//if execution reaches here
			//it indicates the derby database has shutdown
			//and thrown SQLException as expected
			gotSQLExc = true;
			assertTrue(gotSQLExc);
		}
	}
}
