package operations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dbserver.DerbyDatabase;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.Customer;
import model.Product;

public class TestProductOperations {
	
	@BeforeClass
	public static void setup() throws Exception {
		DerbyDatabase.startDerbyDatabaseServer(); 
	}
	
	@Before
	public void clearDatabaseData() {
		DerbyDatabase.dropDatabase(); 
	}
	
	@Test
	public void testSingleProduct() throws EntityWithSameNameAlreadyExistException {
		ProductOperations po = new ProductOperations();

		//Test Persist
		Product p1 = new Product("Kebab");
		po.persist(p1);

		//Test Read
		Product p2 = po.get(p1.getIdProduct());
		assertTrue(p1.getIdProduct() == p2.getIdProduct());
		assertTrue(p1.getName().equals(p2.getName()));

		//Test Update
		Product p3 = new Product("Insalata");
		po.update(p1, p3);
		Product p4 = po.get(p1.getIdProduct());
		assertTrue(p1.getIdProduct() == p4.getIdProduct());
		assertTrue(p3.getName().equals(p4.getName()));
		assertTrue(p3.getName().equals(p4.getName()));

		//Test Delete
		po.delete(p4);
		assertNull(po.get(p4.getIdProduct()));
		assertTrue(po.getAll().size() == 0);

		po.close();
	}

	@Test
	public void testMoreProducts() throws EntityWithSameNameAlreadyExistException {
		ProductOperations po = new ProductOperations();

		//Test Persist
		Product p1 = new Product("Kebab");
		Product p2 =new Product("Insalata");
		Product p3 = new Product("Pomodoro");
		po.persist(p1);
		po.persist(p2);
		po.persist(p3);

		//Test Read
		assertTrue(po.getAll().size() == 3);
		Product p4 = po.get(p3.getIdProduct());
		assertTrue(p4.getIdProduct() == p3.getIdProduct());
		assertTrue(p4.getName().equals(p3.getName()));;

		//Test Update
		p3.setName("Cetriolo");
		po.update(p3);
		p4 = po.get(p3.getIdProduct());
		assertTrue(p4.getIdProduct() == p3.getIdProduct());
		assertTrue(p4.getName().equals(p3.getName()));

		p1.setName("Cavolfiori");
		po.update(p1);
		assertTrue(po.getAll().size() == 3);

		//Test Delete
		po.delete(p1);
		assertTrue(po.getAll().size() == 2);
		po.delete(p2);
		assertTrue(po.getAll().size() == 1);
		po.delete(p3);
		assertTrue(po.getAll().size() == 0);

		po.close();
	}
	
	@Test
	public void productRelationshipOneToMany() throws EntityWithSameNameAlreadyExistException {
		ProductOperations po = new ProductOperations();

		//Test Persist
		Product p1 = new Product("Kebab");
		Product ing1 = new Product("Insalata");

		p1.addIngredient(ing1, 1);

		po.persist(ing1);
		po.persist(p1);

		//Test Read
		Product p2 = po.get(p1.getIdProduct());
		Product ing2 = po.get(ing1.getIdProduct());
		assertTrue(p1.getIdProduct() == p2.getIdProduct());
		assertTrue(p1.getName().equals(p2.getName()));
		assertTrue(ing1.getIdProduct() == ing2.getIdProduct());
		assertTrue(ing1.getName().equals(ing2.getName()));
		assertTrue(p2.getIngredients().get(0).getIngredient().equals(ing2));

		//Test Update
		Product ing3 = new Product("Insalata");
		po.update(ing1, ing3);
		Product ing4 = po.get(ing1.getIdProduct());
		assertTrue(ing1.getIdProduct() == ing4.getIdProduct());
		assertTrue(ing3.getName().equals(ing4.getName()));
		assertTrue(p2.getIngredients().get(0).getIngredient().equals(ing4));


		//Test Delete
		po.delete(ing4);
		assertNull(po.get(ing4.getIdProduct()));
		assertNull(po.get(p2.getIdProduct()));

		po.close();
	}
	
	@Test
	public void moreProductRelationshipOneToMany() throws EntityWithSameNameAlreadyExistException {
		ProductOperations po = new ProductOperations();

		//Test Persist
		Product p1 = new Product("Kebab");
		Product ing1 = new Product("Insalata");
		Product ing2 = new Product("Pomodoro");
		Product ing3 = new Product("Pane");

		p1.addIngredient(ing1, 1);
		p1.addIngredient(ing2, 1);
		p1.addIngredient(ing3, 1);

		po.persist(ing1);
		po.persist(ing2);
		po.persist(ing3);
		po.persist(p1);


		//Test Read
		Product p2 = po.get(p1.getIdProduct());
		Product ing4 = po.get(ing1.getIdProduct());
		Product ing5 = po.get(ing2.getIdProduct());
		Product ing6 = po.get(ing3.getIdProduct());
		assertTrue(p1.getIdProduct() == p2.getIdProduct());
		assertTrue(p1.getName().equals(p2.getName()));
		assertTrue(ing1.getIdProduct() == ing4.getIdProduct());
		assertTrue(ing1.getName().equals(ing4.getName()));
		assertTrue(ing2.getIdProduct() == ing5.getIdProduct());
		assertTrue(ing2.getName().equals(ing5.getName()));
		assertTrue(ing3.getIdProduct() == ing6.getIdProduct());
		assertTrue(ing3.getName().equals(ing6.getName()));
		assertTrue(p2.getIngredients().get(0).getIngredient().equals(ing4));
		assertTrue(p2.getIngredients().get(1).getIngredient().equals(ing5));
		assertTrue(p2.getIngredients().get(2).getIngredient().equals(ing6));

		//Test Update
		Product ing7 = new Product("Salsa");
		po.update(ing1, ing7);
		Product ing8 = po.get(ing1.getIdProduct());
		assertTrue(ing1.getIdProduct() == ing8.getIdProduct());
		assertTrue(ing7.getName().equals(ing8.getName()));
		assertTrue(p2.getIngredients().get(0).getIngredient().equals(ing8));


		//Test Delete
		po.delete(ing4);
		assertNull(po.get(ing4.getIdProduct())); 
		assertNull(po.get(p2.getIdProduct()));
		
		po.delete(ing5);
		po.delete(ing6);
		assertTrue(po.getAll().size() == 0);

		po.close();
	}
	
	@Test
	public void testIngredientsInChain() throws EntityWithSameNameAlreadyExistException {
		ProductOperations po = new ProductOperations();
		
		//Test Persist
		Product ing1 = new Product("Insalata");
		Product p1 = new Product("Kebab");
		Product p2 = new Product("PizzaConKebab");
		p1.addIngredient(ing1, 3);
		p2.addIngredient(p1, 1);
		po.persist(p2);
		
		//Test Read
		Product ing_1 = po.get(ing1.getIdProduct());
		Product p_1 = po.get(p1.getIdProduct());
		Product p_2 = po.get(p2.getIdProduct());
		assertTrue(ing_1.getName().equals(ing1.getName()));
		assertTrue(p_1.getName().equals(p1.getName()));
		assertTrue(p_2.getName().equals(p2.getName()));
		assertTrue(p2.getIngredients().size() == 1);
		assertTrue(p2.getIngredients().get(0).getIngredient().equals(p1));
		assertTrue(p1.getIngredients().size() == 1);
		assertTrue(p1.getIngredients().get(0).getIngredient().equals(ing1));
		assertTrue(ing1.getIngredients().size() == 0);
		assertTrue(po.getAll().size() == 3);
		
		//Test Update
		Product ing2 = new Product("Pomodoro");
		po.update(ing_1, ing2);
		Product ing_2 = po.get(ing2.getIdProduct());
		assertTrue(ing_2.getName().equals(ing2.getName()));
		
		//Test Delete
		po.delete(ing_2);
		assertNull(po.get(ing_2.getIdProduct()));
		assertNull(po.get(p_1.getIdProduct()));
		assertNull(po.get(p_2.getIdProduct()));
		assertTrue(po.getAll().size() == 0);
		
		po.close();
	}
	
	@Test
	public void testUniqueProduct() throws EntityWithSameNameAlreadyExistException {
		ProductOperations po = new ProductOperations();
		Product p1 = new Product("Kebab");
		po.persist(p1);
		Product p2 = new Product("Kebab");
		boolean gotException = false;
		try {
			po.persist(p2);
			fail("The same product was persisted twice. "
					+ "EntityWithSameNameAlreadyExistException wasn't thrown");
		} catch (EntityWithSameNameAlreadyExistException e) {
			gotException = true;
			assertTrue(gotException);
		} finally {
			po.close();
		}
	}
	
	@AfterClass
	public static void tearDown() {
		boolean gotSQLExc = false;
		try {
			DerbyDatabase.dropDatabase();
			DerbyDatabase.stopDerbyDatabaseServer();
			fail("Derby Database didn't throw SQLException");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			//if execution reaches here
			//it indicates the derby database has shutdown
			//and thrown SQLException as expected
			gotSQLExc = true;
			assertTrue(gotSQLExc);
		}
	}
	
}
