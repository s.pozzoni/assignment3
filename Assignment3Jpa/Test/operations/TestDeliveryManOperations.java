package operations;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import dbserver.DerbyDatabase;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.Customer;
import model.DeliveryMan;
import model.Driver;

public class TestDeliveryManOperations {
	
	@BeforeClass
	public static void setup() throws Exception {
		DerbyDatabase.startDerbyDatabaseServer(); 
	}
	
	@Before
	public void clearDatabaseData() {
		DerbyDatabase.dropDatabase(); 
	}
	
	@Test
	public void testSingleDeliveryMan() throws EntityWithSameNameAlreadyExistException {

		DeliveryManOperations dop = new DeliveryManOperations();

		//Test Persist
		DeliveryMan d1 = new Driver();
		d1.setFirstName("Luciano");
		d1.setLastName("Spalletti");
		dop.persist(d1);

		//Test Read
		DeliveryMan d2 = dop.get(d1.getIdDeliveryMan());
		assertTrue(d1.getIdDeliveryMan() == d2.getIdDeliveryMan());
		assertTrue(d1.getFirstName().equals(d2.getFirstName()));
		assertTrue(d1.getLastName().equals(d2.getLastName()));

		//Test Update
		DeliveryMan d3 = new Driver("Antonio", "Conte");
		dop.update(d1, d3);
		DeliveryMan d4 = dop.get(d1.getIdDeliveryMan());
		assertTrue(d1.getIdDeliveryMan() == d4.getIdDeliveryMan());
		assertTrue(d3.getFirstName().equals(d4.getFirstName()));
		assertTrue(d3.getLastName().equals(d4.getLastName()));

		//Test Delete
		dop.delete(d4);
		assertNull(dop.get(d4.getIdDeliveryMan()));
		assertTrue(dop.getAll().size() == 0);

		dop.close();
	}

	@Test
	public void testMoreDeliveryMans() throws EntityWithSameNameAlreadyExistException {

		DeliveryManOperations dop = new DeliveryManOperations();

		//Test Persist
		DeliveryMan d1 = new Driver("Luciano","Spalletti");
		DeliveryMan d2 = new Driver("Ringhio","Gattuso");
		DeliveryMan d3 = new Driver("Max","Mazzarri");
		dop.persist(d1);
		dop.persist(d2);
		dop.persist(d3);

		//Test Read
		assertTrue(dop.getAll().size() == 3);
		DeliveryMan d4 = dop.get(d3.getIdDeliveryMan());
		assertTrue(d4.getIdDeliveryMan() == d3.getIdDeliveryMan());
		assertTrue(d4.getFirstName().equals(d3.getFirstName()));
		assertTrue(d4.getLastName().equals(d3.getLastName()));

		//Test Update
		d3.setFirstName("Walter");
		dop.update(d3);
		d4 = dop.get(d3.getIdDeliveryMan());
		assertTrue(d4.getIdDeliveryMan() == d3.getIdDeliveryMan());
		assertTrue(d4.getFirstName().equals(d3.getFirstName()));
		assertTrue(d4.getLastName().equals(d3.getLastName()));

		d1.setFirstName("Antonio");
		d1.setLastName("Conte");
		dop.update(d1);
		assertTrue(dop.getAll().size() == 3);

		//Test Delete
		dop.delete(d1);
		assertTrue(dop.getAll().size() == 2);
		dop.delete(d2);
		assertTrue(dop.getAll().size() == 1);
		dop.delete(d3);
		assertTrue(dop.getAll().size() == 0);

		dop.close();

	}
	
	@Test
	public void testUniqueDeliveryMan() throws EntityWithSameNameAlreadyExistException {
		DeliveryManOperations dop = new DeliveryManOperations();
		DeliveryMan d1 = new Driver("Luciano", "Spalletti");
		dop.persist(d1);
		DeliveryMan d2 = new Driver("Luciano","Spalletti");
		boolean gotException = false;
		try {
			dop.persist(d2);
			fail("The same deliveryman was persisted twice. "
					+ "EntityWithSameNameAlreadyExistException wasn't thrown");
		} catch (EntityWithSameNameAlreadyExistException e) {
			gotException = true;
			assertTrue(gotException);
		} finally {
			dop.close();
		}
	}
	
	@AfterClass
	public static void tearDown() {
		boolean gotSQLExc = false;
		try {
			DerbyDatabase.dropDatabase();
			DerbyDatabase.stopDerbyDatabaseServer();
			fail("Derby Database didn't throw SQLException");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			//if execution reaches here
			//it indicates the derby database has shutdown
			//and thrown SQLException as expected
			gotSQLExc = true;
			assertTrue(gotSQLExc);
		}
	}
}
