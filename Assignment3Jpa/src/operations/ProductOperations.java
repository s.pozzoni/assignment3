package operations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dao.ProductDao;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.FoodOrder;
import model.OrderDetail;
import model.Product;

public class ProductOperations extends BaseOperations<Product>{
	
	private List<String> entityAttributes;
	private ProductDao productDao;

	public ProductOperations() {
		super(Product.class); 
		productDao = new ProductDao();
		entityAttributes = new ArrayList<>();
		entityAttributes.add(Product.NAME);
		entityAttributes.add(Product.QUANTITY);
	}
	
	@Override
	public void persist(Product entity) throws EntityWithSameNameAlreadyExistException {
		if(findBy(Product.NAME, entity.getName()).size()>0)
			throw new EntityWithSameNameAlreadyExistException();
		super.persist(entity);
	}
	
	@Override
	public Product get(long id) {
		baseDao.beginTransaction();
		Product p = baseDao.get(id);
		baseDao.commitTransaction();
		return p;
	}
	
	@Override
	public List<Product> findBy(String attribute, Object value) throws IllegalArgumentException {
		if(!entityAttributes.contains(attribute))
			throw new IllegalArgumentException("Attribute doesn't belong to Product entity");
		if(attribute.equals(Product.NAME)) {
			//Find the specific product with that name
			return super.findBy(new String[]{attribute}, new Object[]{value});
		} else { //attribute is Quantity
			//Find the products which have ingredients in the given quantity
			return productDao.findByQuantity((int)value);
		}
	}
	
	public List<Product> findByIngredient(String ingredientName) throws IllegalArgumentException {
		//Find the products which have as ingredient this specific ingredient
		if(ingredientName == null || ingredientName.equals(""))
			throw new IllegalArgumentException("Invalid ingredient name");
		return productDao.findByIngredient(ingredientName);
	}
	
	public List<Product> findByIngredient(String ingredientName, int quantity) throws IllegalArgumentException {
		if(quantity <= 0)
			throw new IllegalArgumentException("Quantity must be positive");
		if(ingredientName == null || ingredientName.equals(""))
			throw new IllegalArgumentException("Invalid ingredient name");
		//Find products which have as ingredient the given ingredient in the given quantity
		return productDao.findByIngredientAndQuantity(ingredientName, quantity);
	}
	
	@Override
	public List<Product> findBy(String[] attributes, Object[] values) throws IllegalArgumentException {
		if(attributes.length > entityAttributes.size())
			throw new IllegalArgumentException("Too many attributes");
		//Check the attributes are the ones expected and not given twice
		//Also get the index of the attributes in the array
		int principalProductIndex = -1, ingredientQuantityIndex = -1;
		for(int i=0; i<attributes.length; i++) {
			if(!entityAttributes.contains(attributes[i]))
				throw new IllegalArgumentException("Attribute doesn't belong to Product entity");
			if(attributes[i] == Product.NAME && principalProductIndex == -1)
				principalProductIndex = i;
			else if(attributes[i] == Product.NAME && principalProductIndex != -1)
				throw new IllegalArgumentException("Attribute NAME is given twice");
			else if(attributes[i] == Product.QUANTITY && ingredientQuantityIndex == -1)
				ingredientQuantityIndex = i;
			else throw new IllegalArgumentException("Attribute QUANTITY is given twice");
		}
		//Find the ingredients of the given principal product in the given quantity 
		String principalProductName = (String)values[principalProductIndex];
		int ingredientQuantity = (int)values[ingredientQuantityIndex];
		return productDao.findBy(principalProductName, ingredientQuantity);
	}
	
	
	@Override
	public Product update(Product oldEntity, Product newEntity) {
		baseDao.beginTransaction();
		
		Product product = baseDao.get(oldEntity.getIdProduct());
		if(product != null) {
			newEntity.setIdProduct(product.getIdProduct());
			product = baseDao.update(newEntity);
		}
		baseDao.commitTransaction();
		return product;		
	}
	
	//Se il prodotto  ingrediente di altri prodotti dobbiamo eliminare quei prodotti.
	@Override	
	public void delete(Product entity) {
		//Delete every order's details related to this product		
		List<OrderDetail> orderDetails = entity.getOrders();
		List<OrderDetail> orderDetailsToDelete = new ArrayList<>();
		OrderOperations oo = new OrderOperations();
		orderDetails.forEach((od) -> {
			FoodOrder order = oo.get(od.getOrder().getIdOrder());
			if(order != null) {
				//If the order contains other products besides this, delete only the related orderDetail
				if(od.getOrder().getProducts().size() > 1)
					orderDetailsToDelete.add(od);
				//if the order contains only this one product, then delete the order
				else
					oo.delete(order);
			}
		});
		for(OrderDetail od : orderDetailsToDelete)
			od.getOrder().removeProduct(entity);
		oo.close();
		OrderOperations.deleteOrderDetails(orderDetailsToDelete);	
		entity.setOrders(new ArrayList<OrderDetail>());
				
		baseDao.beginTransaction();
		//Delete the products of which the product entity is ingredient of
		deleteProductsMadeOfIngredient(entity.getIdProduct());
		if(!baseDao.getEntityManager().contains(entity))
			entity = baseDao.update(entity);
		baseDao.delete(entity);
		baseDao.commitTransaction();	
	}
	
	private void deleteProductsMadeOfIngredient(long idIngredient) {
		List<Product> products = productDao.deleteProductsMadeOfIngredient(idIngredient);
		products.forEach((p) -> {
			//Delete products of which p is ingredient
			deleteProductsMadeOfIngredient(p.getIdProduct());
			//Delete p
			if(!baseDao.getEntityManager().contains(p))
				p = baseDao.update(p);
			baseDao.delete(p);
		});
	}
	
	interface DeleteProducts {
		void deleteProducts(long idIngredient); 
	}
	
}
