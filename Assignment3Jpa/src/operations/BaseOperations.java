package operations;

import java.util.List;

import dao.BaseDao;
import exceptions.EntityWithSameNameAlreadyExistException;

public abstract class BaseOperations<E> {
	
	protected BaseDao<E> baseDao;
	
	public BaseOperations(Class<E> entityClass) {
		baseDao = new BaseDao<>(entityClass);
	}
	
	//If the same entity is persisted twice with same id, it will get merged automatically with previous record.
	public void persist(E entity) throws EntityWithSameNameAlreadyExistException {
		baseDao.beginTransaction();
		baseDao.persist(entity);
		baseDao.commitTransaction();
	}
	
	public E get(long id) {
		baseDao.beginTransaction();
		E entity = baseDao.get(id);
		baseDao.commitTransaction();
		return entity;
	}
	
	public List<E> getAll() {
		baseDao.beginTransaction();
		List<E> entities = baseDao.getAll();
		baseDao.commitTransaction();
		return entities;
	}
	
	public List<E> findBy(String attribute, Object value) {
		return findBy(new String[]{attribute}, new Object[]{value});		
	}
	
	public List<E> findBy(String[] attributes, Object[] values) {
		baseDao.beginTransaction();
		List<E> entities = baseDao.findBy(attributes, values);
		baseDao.commitTransaction();
		return entities;
	}
	
	public E update(E entity) {
		baseDao.beginTransaction();
		E e = baseDao.update(entity);
		baseDao.commitTransaction();
		return e;
	}
	
	public abstract E update(E oldEntity, E newEntity);
	
	public void delete(E entity) {
		baseDao.beginTransaction();
		baseDao.delete(entity);
		baseDao.commitTransaction();
	}
	
	public void close() {
		baseDao.close();
	}
	
	
}
