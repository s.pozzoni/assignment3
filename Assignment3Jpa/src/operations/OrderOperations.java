package operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dao.BaseDao;
import dao.OrderDao;
import model.FoodOrder;
import model.OrderDetail;
import model.Product;

public class OrderOperations extends BaseOperations<FoodOrder>{
	
	private List<String> entityAttributes;
	private OrderDao orderDao;

	public OrderOperations() {
		super(FoodOrder.class);
		orderDao = new OrderDao();
		entityAttributes = new ArrayList<>();
		entityAttributes.add(FoodOrder.IDFOODORDER);
		entityAttributes.add(FoodOrder.CUSTOMER);
		entityAttributes.add(FoodOrder.RESTAURANT);
		entityAttributes.add(FoodOrder.DELIVERYMAN);
		entityAttributes.add(Product.NAME);
		entityAttributes.add(Product.QUANTITY);
	}
	
	@Override
	public List<FoodOrder> findBy(String attribute, Object value) throws IllegalArgumentException {
		if(!entityAttributes.contains(attribute))
			throw new IllegalArgumentException("Attribute doesn't belong to FoodOrder entity");
		if(attribute.equals(Product.NAME)) {
			//Find orders in which there is this specific product
			return orderDao.findByProduct((String)value);
		} else if(attribute.equals(Product.QUANTITY)) {
			//Find orders in where there are products in the given quantity
			return orderDao.findByProductQuantity((int)value);
		} else return findBy(new String[] {attribute},new Object[] {value}); 
	}
	
	@Override
	public List<FoodOrder> findBy(String[] attributes, Object[] values) throws IllegalArgumentException {
		if(attributes.length > entityAttributes.size())
			throw new IllegalArgumentException("Too many attributes");
		for(String a : attributes)
			if(!entityAttributes.contains(a) && !a.equals(FoodOrder.IDFOODORDER))
				throw new IllegalArgumentException("Attribute doesn't belong to FoodOrder entity");
		List<String> a = Arrays.asList(attributes);
		if(!a.contains(Product.NAME) && !a.contains(Product.QUANTITY))
			//We need only FoodOrder Table
			return super.findBy(attributes, values);
		else {
			//Find Food Orders which have some specific product in the given quantity
			return orderDao.findBy(attributes, values);
		}
	}

	@Override
	public FoodOrder update(FoodOrder oldEntity, FoodOrder newEntity) {
		baseDao.beginTransaction();
		FoodOrder order = baseDao.get(oldEntity.getIdOrder()); 
		if(order != null) {
			newEntity.setIdOrder(order.getIdOrder());
			baseDao.update(newEntity);
		}
		baseDao.commitTransaction();
		return oldEntity;		
	}
	
	@Override
	public void delete(FoodOrder entity) {
		//Remove this order from customer, restaurant and deliveryman		
		baseDao.beginTransaction();
		if(!baseDao.getEntityManager().contains(entity))
			entity = baseDao.update(entity);
		entity.setCustomer(null);
		entity.setRestaurant(null);
		entity.setDeliveryMan(null);
		baseDao.commitTransaction();	
		
		//Remove first every order's details
		deleteOrderDetails(entity.getProducts());
		
		//Remove products from this order
		entity.setProducts(new ArrayList<OrderDetail>());
		
		//Remove the order
		orderDao.beginTransaction();
		orderDao.delete(entity);
		orderDao.commitTransaction();		
	}
	
	public static void deleteOrderDetails(List<OrderDetail> orderDetails) {
		BaseDao<OrderDetail> baseDaoOrderDetail = new BaseDao<>(OrderDetail.class);
		baseDaoOrderDetail.beginTransaction();
		orderDetails.forEach((e) -> { 
			if(!baseDaoOrderDetail.getEntityManager().contains(e))
				e = baseDaoOrderDetail.update(e);
			baseDaoOrderDetail.delete(e); 
		});
		baseDaoOrderDetail.commitTransaction();
		baseDaoOrderDetail.close();
	}
}


