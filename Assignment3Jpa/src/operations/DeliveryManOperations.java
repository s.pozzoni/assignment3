package operations;

import java.util.ArrayList;
import java.util.List;

import exceptions.EntityWithSameNameAlreadyExistException;
import model.DeliveryMan;
import model.FoodOrder;

public class DeliveryManOperations extends BaseOperations<DeliveryMan>{
	
	//If the deliveryman is deleted we chose not to delete also his orders
	//Because maybe the deliveryman has not benn assigned to that order yet
	
	private List<String> entityAttributes;

	public DeliveryManOperations() {
		super(DeliveryMan.class);
		entityAttributes = new ArrayList<>();
		entityAttributes.add(DeliveryMan.FIRSTNAME);
		entityAttributes.add(DeliveryMan.LASTNAME);
		entityAttributes.add(DeliveryMan.VEHICLE);
	}
	
	@Override
	public void persist(DeliveryMan entity) throws EntityWithSameNameAlreadyExistException {
		String[] attributes = new String[]{DeliveryMan.FIRSTNAME, DeliveryMan.LASTNAME};
		String[] values = new String[]{entity.getFirstName(), entity.getLastName()};
		if(findBy(attributes, values).size()>0)
			throw new EntityWithSameNameAlreadyExistException();
		super.persist(entity);
	}
	
	@Override
	public List<DeliveryMan> findBy(String attribute, Object value) throws IllegalArgumentException {
		if(!entityAttributes.contains(attribute))
			throw new IllegalArgumentException("Attribute doesn't belong to DeliveryMan entity");
		return super.findBy(attribute, value);
	}
	
	@Override
	public List<DeliveryMan> findBy(String[] attributes, Object[] values) throws IllegalArgumentException {
		if(attributes.length > entityAttributes.size())
			throw new IllegalArgumentException("Too many attributes");
		for(String a : attributes)
			if(!entityAttributes.contains(a))
				throw new IllegalArgumentException("Attribute doesn't belong to DeliveryMan entity");
		return super.findBy(attributes, values);
	}

	@Override
	public DeliveryMan update(DeliveryMan oldEntity, DeliveryMan newEntity) {
		if(!oldEntity.getClass().getName().equals(newEntity.getClass().getName()))
			throw new ClassCastException();
		baseDao.beginTransaction();
		
		DeliveryMan deliveryMan = baseDao.get(oldEntity.getIdDeliveryMan());
		if (deliveryMan != null) {
			newEntity.setIdDeliveryMan(deliveryMan.getIdDeliveryMan());
			baseDao.update(newEntity);
		}
		baseDao.commitTransaction();
		return oldEntity;	
	}
	
	@Override
	public void delete(DeliveryMan entity) {
		//Remove first every order of the deliveryman
		List<FoodOrder> orders = entity.getOrders();
		OrderOperations oo = new OrderOperations();
		
		orders.forEach((o) -> {
			o.setDeliveryMan(null);
			if(oo.get(o.getIdOrder()) != null)
				oo.update(o);
		});
		
		oo.close();
		entity.setOrders(new ArrayList<FoodOrder>());
		
		//Remove the deliveryman
		baseDao.beginTransaction();
		if(!baseDao.getEntityManager().contains(entity))
			entity = baseDao.update(entity);
		baseDao.delete(entity);
		baseDao.commitTransaction();
	}
	
	
}
