package operations;

import java.util.ArrayList;
import java.util.List;

import exceptions.EntityWithSameNameAlreadyExistException;
import model.FoodOrder;
import model.Restaurant;

public class RestaurantOperations extends BaseOperations<Restaurant> {
	
	private List<String> entityAttributes;
	
	public RestaurantOperations() {
		super(Restaurant.class);
		entityAttributes = new ArrayList<>();
		entityAttributes.add(Restaurant.NAME);
	}
	
	@Override
	public void persist(Restaurant entity) throws EntityWithSameNameAlreadyExistException {
		String[] attributes = new String[]{Restaurant.NAME};
		String[] values = new String[]{entity.getName()};
		if(findBy(attributes, values).size()>0)
			throw new EntityWithSameNameAlreadyExistException();
		super.persist(entity);
	}
	
	@Override
	public List<Restaurant> findBy(String attribute, Object value) throws IllegalArgumentException {
		if(!entityAttributes.contains(attribute))
			throw new IllegalArgumentException("Attribute doesn't belong to Restaurant entity");
		return super.findBy(attribute, value);
	}
	
	@Override
	public List<Restaurant> findBy(String[] attributes, Object[] values) throws IllegalArgumentException {
		if(attributes.length > entityAttributes.size())
			throw new IllegalArgumentException("Too many attributes");
		for(String a : attributes)
			if(!entityAttributes.contains(a))
				throw new IllegalArgumentException("Attribute doesn't belong to Restaurant entity");
		return super.findBy(attributes, values);
	}

	@Override
	public Restaurant update(Restaurant oldEntity, Restaurant newEntity) {
		baseDao.beginTransaction();
		Restaurant restaurant = baseDao.get(oldEntity.getIdRestaurant());
		if (restaurant != null) {
			newEntity.setIdRestaurant(restaurant.getIdRestaurant());
			baseDao.update(newEntity);
		}
		baseDao.commitTransaction();
		return oldEntity;
	}
	
	@Override
	public void delete(Restaurant entity) {
		//Remove first every order of the restaurant
		List<FoodOrder> orders = entity.getOrders();
		OrderOperations oo = new OrderOperations();
		orders.forEach((o) -> {
			if(oo.get(o.getIdOrder()) != null)
				oo.delete(o);
		});
		oo.close();
		entity.setOrders(new ArrayList<FoodOrder>());
		
		//Remove the restaurant
		baseDao.beginTransaction();
		if(!baseDao.getEntityManager().contains(entity))
			entity = baseDao.update(entity);
		baseDao.delete(entity);
		baseDao.commitTransaction();
	}

}
