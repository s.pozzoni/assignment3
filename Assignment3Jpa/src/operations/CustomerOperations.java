package operations;

import java.util.ArrayList;
import java.util.List;

import exceptions.EntityWithSameNameAlreadyExistException;
import model.Customer;
import model.FoodOrder;

public class CustomerOperations extends BaseOperations<Customer> {
	
	private List<String> entityAttributes;
		
	public CustomerOperations() {
		super(Customer.class);
		entityAttributes = new ArrayList<>();
		entityAttributes.add(Customer.FIRSTNAME);
		entityAttributes.add(Customer.LASTNAME);
	}
	
	@Override
	public void persist(Customer entity) throws EntityWithSameNameAlreadyExistException {
		String[] attributes = new String[]{Customer.FIRSTNAME, Customer.LASTNAME};
		String[] values = new String[]{entity.getFirstName(), entity.getLastName()};
		if(findBy(attributes, values).size()>0)
			throw new EntityWithSameNameAlreadyExistException();
		super.persist(entity);
	}
	
	@Override
	public List<Customer> findBy(String attribute, Object value) throws IllegalArgumentException {
		if(!entityAttributes.contains(attribute))
			throw new IllegalArgumentException("Attribute doesn't belong to Customer entity");
		return super.findBy(attribute, value);
	}
	
	@Override
	public List<Customer> findBy(String[] attributes, Object[] values) throws IllegalArgumentException {
		if(attributes.length > entityAttributes.size())
			throw new IllegalArgumentException("Too many attributes");
		for(String a : attributes)
			if(!entityAttributes.contains(a))
				throw new IllegalArgumentException("Attribute doesn't belong to Customer entity");
		return super.findBy(attributes, values);
	}
	
	@Override
	public Customer update(Customer oldEntity, Customer newEntity) {
		baseDao.beginTransaction();		
		Customer customer = baseDao.get(oldEntity.getIdCustomer());
		if(customer!=null) {
			newEntity.setIdCustomer(customer.getIdCustomer());
			baseDao.update(newEntity);
		}
		baseDao.commitTransaction();
		return oldEntity;
		
	}
	
	@Override
	public void delete(Customer entity) {
		//Remove first every order of the customer
		List<FoodOrder> orders = entity.getOrders();
		OrderOperations oo = new OrderOperations();
		orders.forEach((o) -> {
			if(oo.get(o.getIdOrder()) != null)
				oo.delete(o);
		});
		oo.close();
		entity.setOrders(new ArrayList<FoodOrder>());
		
		//Remove the customer
		baseDao.beginTransaction();
		if(!baseDao.getEntityManager().contains(entity))
			entity = baseDao.update(entity);
		baseDao.delete(entity);
		baseDao.commitTransaction();
	}
	
}
