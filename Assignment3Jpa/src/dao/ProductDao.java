package dao;

import java.util.List;

import javax.persistence.Query;

import model.Product;

public class ProductDao extends BaseDao<Product> {

	public ProductDao() {
		super(Product.class);
	}
	
	//Find the ingredients of the given principal product in the given quantity
	public List<Product> findBy(String principalProductName, int ingredientQuantity) {
		List<Product> products = super.findBy(new String[]{Product.NAME}, new Object[]{principalProductName});
		if(products.isEmpty())
			return products;
		Product	principalProduct = products.get(0);
		Query q = entityManager.createNativeQuery("SELECT p.idproduct, p.name FROM Product p, ProductComposition pc"
				+ " WHERE p.idproduct = pc.ingredient_idproduct AND "
				+ "pc.principalproduct_idproduct = "+principalProduct.getIdProduct()+" AND "
				+ "pc.quantity = "+ingredientQuantity, Product.class);
		return (List<Product>) q.getResultList();
	}
	
	//Find the products which have ingredients in the given quantity
	public List<Product> findByQuantity(int quantity) {
		Query q = entityManager.createNativeQuery("SELECT DISTINCT p.idproduct, p.name FROM Product p, ProductComposition pc"
							+ " WHERE p.idproduct = pc.principalproduct_idproduct AND "
							+ "pc.quantity = "+quantity, Product.class);
		return (List<Product>) q.getResultList();
	}
	
	//Find the products which have as ingredient this specific ingredient
	public List<Product> findByIngredient(String ingredientName) {
		Query q = entityManager.createNativeQuery("SELECT DISTINCT p.idproduct, p.name FROM Product p, ProductComposition pc"
						+ " WHERE p.idproduct = pc.principalproduct_idproduct AND "
						+ "pc.ingredient_idproduct = (SELECT p.idproduct FROM product p WHERE p.name = '"+ingredientName+"')", Product.class);
		return (List<Product>) q.getResultList();
	}
	
	//Find products which have as ingredient the given ingredient in the given quantity
	public List<Product> findByIngredientAndQuantity(String ingredientName, int quantity) {
		Query q = entityManager.createNativeQuery("SELECT DISTINCT p.idproduct, p.name FROM Product p, ProductComposition pc"
						+ " WHERE p.idproduct = pc.principalproduct_idproduct AND "
						+ "pc.ingredient_idproduct = (SELECT p.idproduct FROM product p WHERE p.name = '"+ingredientName+"')"
						+ "AND pc.quantity = "+quantity, Product.class);
		return (List<Product>) q.getResultList();
	}
	
	
	public List<Product> deleteProductsMadeOfIngredient(long idIngredient) {
		Query q = entityManager.createNativeQuery("SELECT p.idproduct, p.name FROM Product p, ProductComposition pc"
						+ " WHERE p.idproduct = pc.principalproduct_idproduct AND pc.ingredient_idproduct = "+idIngredient, Product.class);
		return (List<Product>) q.getResultList();
	}

}
