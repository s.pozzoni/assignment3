package dao;

import java.util.List;

import javax.persistence.Query;

import model.FoodOrder;
import model.Product;

public class OrderDao extends BaseDao<FoodOrder> {

	public OrderDao() {
		super(FoodOrder.class);
	}
	
	public List<FoodOrder> findByProduct(String productName) {
		Query q = entityManager.createNativeQuery("SELECT o.idfoodorder, o.customerid, o.deliverymanid, o.restaurantid"
						+ " FROM FoodOrder o, OrderDetail od WHERE o.idfoodorder = od.foodorder_idfoodorder AND"
						+ " od.product_idproduct = (SELECT p.idproduct FROM Product p WHERE p.name = '"+productName+"')", FoodOrder.class);
		return (List<FoodOrder>) q.getResultList();
	}
	
	public List<FoodOrder> findByProductQuantity(int quantity) {
		Query q = entityManager.createNativeQuery("SELECT o.idfoodorder, o.customerid, o.deliverymanid, o.restaurantid"
						+ " FROM FoodOrder o, OrderDetail od WHERE o.idfoodorder = od.foodorder_idfoodorder AND"
						+ " od.quantity = "+quantity, FoodOrder.class);
		return (List<FoodOrder>) q.getResultList();
	}
	
	public List<FoodOrder> findBy(String[] attributes, Object[] values) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT o.idfoodorder, o.customerid, o.deliverymanid, o.restaurantid"
						+ " FROM FoodOrder o, OrderDetail od WHERE o.idfoodorder = od.foodorder_idfoodorder AND ");
		for(int i=0; i<attributes.length; i++) {
			switch(attributes[i]) {
				case FoodOrder.IDFOODORDER: sb.append("o.idfoodorder = "+(long)values[i]);
										break;
				case FoodOrder.CUSTOMER: sb.append("o.customerid = "+(long)values[i]); 
									 	break;
				case FoodOrder.DELIVERYMAN: sb.append("o.deliverymanid = "+(long)values[i]);
										break;
				case FoodOrder.RESTAURANT: sb.append("o.restaurantid = "+(long)values[i]);
										break;
				case Product.NAME: sb.append("od.product_idproduct = "+findIdProductByName((String)values[i]));
										break;
				case Product.QUANTITY: sb.append("od.quantity = "+(int)values[i]);
										break;
				default: break;
			}
			if(i+1<attributes.length)
				sb.append(" AND ");
		}
		Query q = entityManager.createNativeQuery(sb.toString(), FoodOrder.class);
		return (List<FoodOrder>) q.getResultList();
	}
	
	public Long findIdProductByName(String productName) {
		Query q1 = entityManager.createNativeQuery("SELECT p.idproduct, p.name FROM Product p "
				+ " WHERE p.name= :namep", Product.class);
		q1.setParameter("namep", productName);
		Product p = (Product) q1.getSingleResult();
		return p.getIdProduct();
	}
	
	@Override
	public void delete(FoodOrder entity) {
		entityManager.createNativeQuery("delete from foodorder where idfoodorder = "+entity.getIdOrder()).executeUpdate();
	}

}
