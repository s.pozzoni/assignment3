package dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

public class BaseDao<E>{
	
	public final static String PERSISTENCE_UNIT_NAME = "Assignment3Jpa";
	
	protected EntityManagerFactory entityManagerFactory;
	
	protected EntityManager entityManager;
	
	protected Class<E> entityClass;
	
	public BaseDao(Class<E> entityClass) {
		entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		entityManager = entityManagerFactory.createEntityManager();
		//Get entity class to avoid make specific dao for every entity
		this.entityClass = entityClass;
	}
	
	public void persist(E entity) {
		entityManager.persist(entity);
	}
	
	public E get(long id) {
		return entityManager.find(entityClass, id);
	}
	
	public List<E> getAll() {
		Query query = entityManager.createNativeQuery("SELECT * FROM "+entityClass.getSimpleName(), entityClass);
		return (List<E>) query.getResultList();
	}
	
	public List<E> findBy(String[] attributes, Object[] values) {
		if(attributes.length != values.length)
			return null;
		CriteriaBuilder cb = entityManager.getCriteriaBuilder(); 
		CriteriaQuery<E> q = cb.createQuery(entityClass);
		Root<E> entity = q.from(entityClass);
		List<Predicate> predicates = new ArrayList<>(values.length);
		for(int i=0; i<values.length; i++)
			if(values[i] != null)
				predicates.add(cb.equal(entity.get(attributes[i]), values[i]));	
		q.select(entity).where(predicates.toArray(new Predicate[predicates.size()]));
		Query query = entityManager.createQuery(q);
		return (List<E>) query.getResultList();
	}
	
	public E update(E entity) {
		return entityManager.merge(entity);
	}
	
	public void delete(E entity) {
		entityManager.remove(entity);
	}
	
	public void beginTransaction() {
        entityManager.getTransaction().begin();
    }

    public void commitTransaction() {
    	entityManager.flush();
        entityManager.getTransaction().commit();
    }

    public void rollbackTransaction() {
        entityManager.getTransaction().rollback();
    }
    
    public boolean isActiveTransaction() {
    	return entityManager.getTransaction().isActive();
    }
    
    public void close() {
    	entityManager.close();
    	entityManagerFactory.close();
    }
	
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
}
