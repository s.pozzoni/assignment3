package dbserver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import dao.BaseDao;
import model.Customer;
import model.DeliveryMan;
import model.FoodOrder;
import model.OrderDetail;
import model.Product;
import model.ProductComposition;
import model.Restaurant;

public class DerbyDatabase {
	
	private static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String JDBC_CONNECT = "jdbc:derby:deliverdb;create=true";
	private static final String JDBC_CLOSE = "jdbc:derby:deliverdb;shutdown=true";
	
	private static DerbyDatabase instance = null;
	private static Connection conn = null;
	
	public DerbyDatabase() throws ClassNotFoundException {
		Class.forName(DRIVER);
	}
	
	public static synchronized void startDerbyDatabaseServer() throws ClassNotFoundException, SQLException {
		if(instance == null)
			instance = new DerbyDatabase();
		conn = DriverManager.getConnection(JDBC_CONNECT);		
	}
	
	public static synchronized void stopDerbyDatabaseServer() throws ClassNotFoundException, SQLException {
		if(instance == null)
			instance = new DerbyDatabase();
		conn.close();
		DriverManager.getConnection(JDBC_CLOSE);
		
	}
	
	public static synchronized void dropDatabase() {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory(BaseDao.PERSISTENCE_UNIT_NAME);
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Query q1 = entityManager.createQuery("DELETE FROM "+Customer.TABLE_NAME);
		Query q2 = entityManager.createQuery("DELETE FROM "+DeliveryMan.TABLE_NAME);
		Query q3 = entityManager.createQuery("DELETE FROM "+FoodOrder.TABLE_NAME);
		Query q4 = entityManager.createQuery("DELETE FROM "+OrderDetail.TABLE_NAME);
		Query q5 = entityManager.createQuery("DELETE FROM "+Product.TABLE_NAME);
		Query q6 = entityManager.createQuery("DELETE FROM "+ProductComposition.TABLE_NAME);
		Query q7 = entityManager.createQuery("DELETE FROM "+Restaurant.TABLE_NAME);
		entityManager.getTransaction().begin();
		q4.executeUpdate();
		q3.executeUpdate();
		q1.executeUpdate();
		q2.executeUpdate();
		q7.executeUpdate();
		q6.executeUpdate();
		q5.executeUpdate();
		entityManager.flush();
		entityManager.getTransaction().commit();
		entityManager.close();
		entityManagerFactory.close();
	}
	
}
