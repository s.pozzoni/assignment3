package exceptions;

public class EntityWithSameNameAlreadyExistException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public EntityWithSameNameAlreadyExistException() {
		super("Record of this entity already exists in the database");
	}

}
