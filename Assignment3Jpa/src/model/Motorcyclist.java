package model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

	@Entity
	@DiscriminatorValue(value = "Motorcyclist")
	public class Motorcyclist extends DeliveryMan {
		
		private String vehicle;

		public Motorcyclist() {
			super();
			this.vehicle = "Motorbike";
		}
		public Motorcyclist(String firstName, String lastName) throws IllegalArgumentException {
			super(firstName, lastName);
			this.vehicle = "Motorbike";
		}
		
		@Override
		public String deliverWith() {
			return vehicle;
		}

	}

