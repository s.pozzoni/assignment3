package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = Product.TABLE_NAME)
public class Product {
	
	public static final String TABLE_NAME = "Product";
	//Fields
	public static final String NAME = "name";
	public static final String QUANTITY = "quantity";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idProduct;
	
	private String name;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE}, mappedBy = "product")
	private List<OrderDetail> orders;
	 
	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL, mappedBy = "ingredient")
	private List<ProductComposition> ingredients;
	
	public Product() {
		orders = new ArrayList<>();
		ingredients= new ArrayList<ProductComposition>();
	}
	
	
	public Product(String name) {
		setName(name);
		orders= new ArrayList<OrderDetail>();
		ingredients= new ArrayList<ProductComposition>();
	}

	public void setProduct(Product p)  {
		name=p.getName();
		orders=p.getOrders();
		ingredients=p.getIngredients();
	}

	public void addIngredient(Product p,int quantity) {
		ProductComposition pc = new ProductComposition(this,p,quantity);
		if(ingredients == null)
			ingredients = new ArrayList<ProductComposition>();
		ingredients.add(pc);
	}
	
	public boolean containIngredient(Product ingredient) {
		ProductComposition pc = new ProductComposition(this, ingredient, 1);
		return ingredients.contains(pc);
	}
	
	public void removeIngredient(Product ingredient) { 
		ProductComposition pc = new ProductComposition(this, ingredient, 1);
		if(!ingredients.contains(pc))
			return;
		ingredients.remove(pc);
	}
	
	public long getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(long idProduct) {
		this.idProduct = idProduct;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws IllegalArgumentException {
		if(name == null || name.equals(""))
			throw new IllegalArgumentException("Invalid argument for Product");
		this.name = name;
	}
	
	public void setOrders(List<OrderDetail> ordersDetail) {
		this.orders = ordersDetail;
	}

	public List<OrderDetail> getOrders() {
		return orders;
	}

	public List<ProductComposition> getIngredients() {
		return ingredients;
	}
	
	public void setIngredients(List<ProductComposition> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idProduct);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		return idProduct == other.idProduct;
	}

	@Override
	public String toString() {
		return "Product [idProduct=" + idProduct + ", name=" + name + ", ingredients="
				+ ingredients + "]";
	}
	
	
}
