package model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import model.idclass.ProductCompositionId;

@Entity(name = ProductComposition.TABLE_NAME)
@IdClass(ProductCompositionId.class)
public class ProductComposition {
	
	public static final String TABLE_NAME = "ProductComposition";
	
	@Id
	@ManyToOne (fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn(name = "PrincipalProductID")
	private Product principalProduct;
	
	@Id
	@ManyToOne (fetch = FetchType.LAZY)
	@PrimaryKeyJoinColumn(name = "IngredientID")
	private Product ingredient;
    
    private int quantity;
    
    public ProductComposition() {}

	public ProductComposition(Product principalProduct, Product ingredient, int quantity) {
		this.principalProduct = principalProduct;
		this.ingredient = ingredient;
		setQuantity(quantity);
	}

	public Product getPrincipalProduct() {
		return principalProduct;
	}

	public void setPrincipalProduct(Product principalProduct) {
		this.principalProduct = principalProduct;
	}

	public Product getIngredient() {
		return ingredient;
	}

	public void setIngredient(Product ingredient) {
		this.ingredient = ingredient;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) throws IllegalArgumentException {
		if(quantity <= 0)
			throw new IllegalArgumentException("Quantity value must be positive");
		this.quantity = quantity;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(ingredient, principalProduct);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductComposition other = (ProductComposition) obj;
		return Objects.equals(ingredient, other.ingredient) && Objects.equals(principalProduct, other.principalProduct);
	}

	@Override
	public String toString() {
		return "Ingredient["+ingredient + ", quantity = "+quantity +" ]";
	}
    	
}
