package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

@Entity(name=DeliveryMan.TABLE_NAME)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DELIVERYMAN_TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class DeliveryMan {
	
	public static final String TABLE_NAME = "DeliveryMan";
	//Fields
	public static final String FIRSTNAME = "firstName";
	public static final String LASTNAME = "lastName";
	public static final String VEHICLE = "vehicle";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idDeliveryMan;
	
	private String firstName;
	
	private String lastName;
	
	@OneToMany(cascade = {CascadeType.MERGE}, mappedBy = "deliveryMan", orphanRemoval = true, fetch = FetchType.EAGER)
	private List<FoodOrder> orders;

	public DeliveryMan() {
		orders = new ArrayList<FoodOrder>();
	}

	public DeliveryMan(String firstName, String lastName) throws IllegalArgumentException {
		setFirstName(firstName);
		setLastName(lastName);
		this.orders = new ArrayList<FoodOrder>();
	}

	public long getIdDeliveryMan() {
		return idDeliveryMan;
	}

	public void setIdDeliveryMan(long idDeliveryMan) {
		this.idDeliveryMan = idDeliveryMan;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) throws IllegalArgumentException {
		if(firstName == null || firstName.equals(""))
			throw new IllegalArgumentException("Invalid argument for DeliveryMan");
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) throws IllegalArgumentException {
		if(lastName == null || lastName.equals(""))
			throw new IllegalArgumentException("Invalid argument for DeliveryMan");
		this.lastName = lastName;
	}
	
	public List<FoodOrder> getOrders() {
		return orders;
	}

	public void setOrders(List<FoodOrder> orders) {
		this.orders = orders;
	}
	
	protected void addOrder(FoodOrder order) {
		if(orders == null)
			orders = new ArrayList<FoodOrder>();
		else if(orders.contains(order))
			return;
		orders.add(order);
		order.setDeliveryMan(this);
	}
	
	public void removeOrder(FoodOrder order) {
		if(orders == null) {
			orders = new ArrayList<FoodOrder>();
			return;
		}
		if(!orders.contains(order))
			return;
		orders.remove(order);
		order.setDeliveryMan(null);
	}

	public abstract String deliverWith();

	@Override
	public int hashCode() {
		return Objects.hash(idDeliveryMan);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliveryMan other = (DeliveryMan) obj;
		return idDeliveryMan == other.idDeliveryMan;
	}

	@Override
	public String toString() {
		return "DeliveryMan [firstName=" + firstName + ", lastName=" + lastName + ", orders=" + orders + "]";
	}
	
}
