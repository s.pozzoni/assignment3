package model.idclass;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OrderDetailId implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name = "foodOrderId")
    private Long foodOrderId;
 
    @Column(name = "productId")
    private Long productId;

    
    public OrderDetailId() {
		this.foodOrderId = null;
		this.productId = null;
	}
    
	public OrderDetailId(Long orderId, Long productId) {
		this.foodOrderId = orderId;
		this.productId = productId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(foodOrderId, productId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderDetailId other = (OrderDetailId) obj;
		return Objects.equals(foodOrderId, other.foodOrderId) && Objects.equals(productId, other.productId);
	}

	@Override
	public String toString() {
		return "OrderDetailId [foodOrderId=" + foodOrderId + ", productId=" + productId + "]";
	} 
	
}
