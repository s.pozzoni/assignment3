package model.idclass;

import java.io.Serializable;
import java.util.Objects;

public class ProductCompositionId implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private long principalProduct;
	
	private long ingredient;
	
	@Override
	public int hashCode() {
		return Objects.hash(ingredient, principalProduct);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductCompositionId other = (ProductCompositionId) obj;
		return ingredient == other.ingredient && principalProduct == other.principalProduct;
	}

}
