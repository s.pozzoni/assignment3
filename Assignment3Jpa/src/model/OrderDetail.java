package model;

import java.util.Objects;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import model.idclass.OrderDetailId;

@Entity(name = OrderDetail.TABLE_NAME)
public class OrderDetail {
	
	public static final String TABLE_NAME = "OrderDetail";
	
	@EmbeddedId
	private OrderDetailId id;
	
	@ManyToOne
    @MapsId("foodOrderId")
    private FoodOrder foodOrder;
	
    @ManyToOne
    @MapsId("productId")
    private Product product;
	
	private int quantity;
	
	public OrderDetail() {
		this.foodOrder=null;
		this.product=null;
		this.id=new OrderDetailId(null,null);
		
	}

	public OrderDetail(FoodOrder order, Product product,int quantity) throws IllegalArgumentException {
		this.foodOrder=order;
		this.product=product;
		setQuantity(quantity);
		this.id=new OrderDetailId(order.getIdOrder(),product.getIdProduct());		
	}
	
	public OrderDetailId getId() {
		return id;
	}

	public void setId(OrderDetailId id) {
		this.id = id;
	}

	public FoodOrder getOrder() {
		return foodOrder;
	}

	public void setOrder(FoodOrder order) {
		this.foodOrder = order;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) throws IllegalArgumentException {
		if(quantity <= 0)
			throw new IllegalArgumentException("Quantity value must be positive");
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		return Objects.hash(foodOrder, product);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderDetail other = (OrderDetail) obj;
		return Objects.equals(foodOrder, other.foodOrder) && Objects.equals(product, other.product);
	}

	@Override
	public String toString() {
		return "OrderDetail [idOrderDetail: "+id+" , idFoodOrder = "+foodOrder.getIdOrder()+
				" ,idProduct = "+product.getIdProduct()+" ,Product: "+product.getName()+" ]";
	}
    
}