package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name = Restaurant.TABLE_NAME)
public class Restaurant {
	
	public static final String TABLE_NAME = "Restaurant";
	//Fields
	public static final String NAME = "name";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idRestaurant;
	
	private String name;
	
	@OneToMany(cascade = {CascadeType.MERGE}, mappedBy = "restaurant", orphanRemoval = true, fetch = FetchType.EAGER)
	private List<FoodOrder> orders;
	
	public Restaurant() {
		orders = new ArrayList<FoodOrder>();
	}
	public Restaurant(String name) throws IllegalArgumentException {
		setName(name);
		orders = new ArrayList<FoodOrder>();
	}

	public long getIdRestaurant() {
		return idRestaurant;
	}

	public void setIdRestaurant(long idRestaurant) {
		this.idRestaurant = idRestaurant;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) throws IllegalArgumentException {
		if(name == null || name.equals(""))
			throw new IllegalArgumentException("Invalid argument for Restaurant");
		this.name = name;
	}

	public List<FoodOrder> getOrders() {
		return orders;
	}

	public void setOrders(List<FoodOrder> orders) {
		this.orders = orders;
	}
	

	public void addOrder(FoodOrder order) {
		if(orders == null)
			orders = new ArrayList<FoodOrder>();
		else if(orders.contains(order))
			return;
		orders.add(order);
		order.setRestaurant(this);
	}
	
	public void removeOrder(FoodOrder order) {
		if(orders == null) {
			orders = new ArrayList<FoodOrder>();
			return;
		}
		if(!orders.contains(order))
			return;
		orders.remove(order);
		order.setRestaurant(null);
	}

	@Override
	public int hashCode() {
		return Objects.hash(idRestaurant);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Restaurant other = (Restaurant) obj;
		return idRestaurant == other.idRestaurant;
	}

	@Override
	public String toString() {
		return "Restaurant [idRestaurant=" + idRestaurant + ", name=" + name + ", orders=" + orders + "]";
	}
	
}
