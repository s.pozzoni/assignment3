package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name=Customer.TABLE_NAME)
public class Customer {
	
	public static final String TABLE_NAME = "Customer";
	//Fields
	public static final String FIRSTNAME = "firstName";
	public static final String LASTNAME = "lastName";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idCustomer;
	
	private String firstName;
	
	private String lastName;
	
	@OneToMany(cascade = {CascadeType.MERGE}, mappedBy = "customer", orphanRemoval = true, fetch = FetchType.EAGER)
	private List<FoodOrder> orders;
	
	public Customer() {
		orders = new ArrayList<FoodOrder>();
	}
	
	public Customer(Customer c) {
		firstName=c.getFirstName();
		lastName=c.getLastName();
		orders=c.getOrders();
	}
	
	public Customer(String firstName, String lastName) throws IllegalArgumentException {
		setFirstName(firstName);
		setLastName(lastName);
		orders = new ArrayList<FoodOrder>();
	}

	public long getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(long idCustomer) {
		this.idCustomer = idCustomer;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) throws IllegalArgumentException {
		if(firstName == null || firstName.equals(""))
			throw new IllegalArgumentException("Invalid argument for Customer");
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) throws IllegalArgumentException {
		if(lastName == null || lastName.equals(""))
			throw new IllegalArgumentException("Invalid argument for Customer");
		this.lastName = lastName;
	}

	public List<FoodOrder> getOrders() {
		return orders;
	}

	public void setOrders(List<FoodOrder> orders) {
		this.orders = orders;
	}
	
	protected void addOrder(FoodOrder order) {
		if(orders == null)
			orders = new ArrayList<FoodOrder>();
		else if(orders.contains(order))
			return;
		orders.add(order);
		order.setCustomer(this);
	}
	
	public void removeOrder(FoodOrder order) {
		if(orders == null) {
			orders = new ArrayList<FoodOrder>();
			return;
		}
		if(!orders.contains(order))
			return;
		orders.remove(order);
		order.setCustomer(null);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(idCustomer);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		return idCustomer == other.idCustomer;
	}

	@Override
	public String toString() {
		return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", orders=" + orders + "]";
	}
	
}
