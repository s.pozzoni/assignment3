package model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Rider")
public class Rider extends DeliveryMan {

	private String vehicle;

	public Rider() {
		super();
		this.vehicle = "Bike";
	}
	public Rider(String firstName, String lastName) throws IllegalArgumentException {
		super(firstName, lastName);
		this.vehicle = "Bike";
	}
	@Override
	public String deliverWith() {
		return vehicle;
	}

}
