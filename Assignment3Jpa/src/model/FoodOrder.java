package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity(name=FoodOrder.TABLE_NAME)
public class FoodOrder {
	
	public static final String TABLE_NAME = "FoodOrder";
	//Fields
	public static final String IDFOODORDER = "idFoodOrder";
	public static final String CUSTOMER = "customer";
	public static final String RESTAURANT = "restaurant";
	public static final String DELIVERYMAN = "deliveryMan";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idFoodOrder;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CustomerID",referencedColumnName="idCustomer")
	private Customer customer;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RestaurantID",referencedColumnName="idRestaurant")
	private Restaurant restaurant;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "DeliverymanID",referencedColumnName="idDeliveryMan", nullable = true)
	private DeliveryMan deliveryMan;
	
	@OneToMany(cascade = {CascadeType.MERGE}, mappedBy = "foodOrder")
	private List<OrderDetail> products;
	
	
	public FoodOrder() {
		products=new ArrayList<OrderDetail>();
	}
	
	public void addProduct(Product p,int quantity) throws IllegalArgumentException {
		OrderDetail o = new OrderDetail(this, p, quantity);
		if(products == null)
			products=new ArrayList<OrderDetail>();
		products.add(o);
		List<OrderDetail> orderDetails = p.getOrders();
		if(orderDetails.contains(o))
			return;
		orderDetails.add(o);
		p.setOrders(orderDetails);
	}
	
	public boolean containProduct(Product p) {
		OrderDetail o = new OrderDetail(this, p, 1);
		return products.contains(o);
	}
	
	public OrderDetail removeProduct(Product p) {
		OrderDetail o = new OrderDetail(this, p, 1);
		products.remove(o);
		List<OrderDetail> orderDetails = p.getOrders();
		if(!orderDetails.contains(o))
			return null;
		orderDetails.remove(o);
		p.setOrders(orderDetails);
		return o;
	}
	
	public long getIdOrder() {
		return idFoodOrder;
	}

	public void setIdOrder(long idOrder) {
		this.idFoodOrder = idOrder;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		if (this.customer == null ? (customer == null) : this.customer.equals(customer))
			return;
		Customer oldCustomer = this.customer;
		this.customer = customer;
		
		if(oldCustomer != null)
			oldCustomer.removeOrder(this);
		
		if(customer != null)
			customer.addOrder(this);
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		if (this.restaurant == null ? (restaurant == null) : this.restaurant.equals(restaurant))
			return;
		Restaurant oldRestaurant = this.restaurant;
		this.restaurant = restaurant;
		
		if(oldRestaurant != null)
			oldRestaurant.removeOrder(this);
		
		if(restaurant != null)
			restaurant.addOrder(this);
	}

	public DeliveryMan getDeliveryMan() {
		return deliveryMan;
	}

	public void setDeliveryMan(DeliveryMan deliveryMan) {
		if (this.deliveryMan == null ? (deliveryMan == null) : this.deliveryMan.equals(deliveryMan))
			return;
		this.deliveryMan = deliveryMan;
		
		if(deliveryMan != null)
			deliveryMan.addOrder(this);
	}

	public List<OrderDetail> getProducts() {
		return products;
	}

	public void setProducts(List<OrderDetail> products) {
		this.products = products;
	}

	@Override
	public int hashCode() {
		return Objects.hash(idFoodOrder);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FoodOrder other = (FoodOrder) obj;
		return idFoodOrder == other.idFoodOrder;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Order [idOrder = "+idFoodOrder);
		if(customer  != null)
			sb.append(", customer = "+customer.getFirstName()+" "+customer.getLastName());
		if(restaurant != null)
			sb.append(", restaurant = "+restaurant.getName());
		if(deliveryMan != null)
			sb.append(", deliveryman = "+deliveryMan.getFirstName()+" "+deliveryMan.getLastName());
		if(products != null && products.size() > 0)
			sb.append(", products = "+products);
		sb.append("]");
		return sb.toString();
	}
	
	
	
}
