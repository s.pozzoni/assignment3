package model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Driver")
public class Driver extends DeliveryMan {
	
	private String vehicle;
	
	public Driver() {
		super();
		this.vehicle = "Car";
	}

	public Driver(String firstName, String lastName) throws IllegalArgumentException {
		super(firstName, lastName);
		this.vehicle = "Car";
	}

	@Override
	public String deliverWith() {
		return vehicle;
	}

}
