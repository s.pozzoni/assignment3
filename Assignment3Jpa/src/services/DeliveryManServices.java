package services;

import java.util.ArrayList;
import java.util.List;

import exceptions.EntityWithSameNameAlreadyExistException;
import model.DeliveryMan;
import model.Driver;
import model.Motorcyclist;
import model.Rider;
import operations.DeliveryManOperations;

public class DeliveryManServices {
	
	enum VEHICLES { CAR, MOTORBIKE, BIKE };
	
	private static DeliveryManServices instance;
	
	private static DeliveryManOperations dmo;
	
	private DeliveryManServices() {
		dmo = new DeliveryManOperations();
	}
	
	public static void newInstance() {
		if(instance == null)
			instance = new DeliveryManServices();
	}
	
	public static DeliveryMan createDeliveryMan(String firstName, String lastName, VEHICLES vehicle) throws IllegalArgumentException {
		newInstance();
		DeliveryMan d;
		switch(vehicle) {
			case CAR: 
				d = new Driver(firstName, lastName);
				break;
			case MOTORBIKE:
				d = new Motorcyclist(firstName, lastName);
				break;
			case BIKE:
				d = new Rider(firstName, lastName);
				break;
			default: throw new IllegalArgumentException("Vehicle not supported: "+vehicle);
		}
		return !addDeliveryMan(d) ? null : d;
	}
	
	protected static DeliveryMan createDeliveryMan(String firstName, String lastName, String vehicle) {
		newInstance();
		DeliveryMan d;
		switch(vehicle) {
			case "Car": d = createDeliveryMan(firstName, lastName, VEHICLES.CAR); break;
			case "Motorbike": d = createDeliveryMan(firstName, lastName, VEHICLES.MOTORBIKE); break;
			case "Bike": d = createDeliveryMan(firstName, lastName, VEHICLES.BIKE); break;
			default: throw new IllegalArgumentException("Vehicle not supported: "+vehicle);
		}
		return d;
	}
	
	private static boolean addDeliveryMan(DeliveryMan deliveryMan) {
		newInstance();
		try {
			dmo.persist(deliveryMan);
		} catch (EntityWithSameNameAlreadyExistException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static List<DeliveryMan> findDeliveryManByFirstName(String firstName) {
		newInstance();
		//Input control
		if(!isValidStringName(firstName))
			return new ArrayList<>();
		List<DeliveryMan> deliveryMans = dmo.findBy(DeliveryMan.FIRSTNAME, firstName);
		return deliveryMans;
	}
	
	public static List<DeliveryMan> findDeliveryManByLastName(String lastName) {
		newInstance();
		//Input control
		if(!isValidStringName(lastName))
			return new ArrayList<>();
		List<DeliveryMan> deliveryMans = dmo.findBy(DeliveryMan.LASTNAME, lastName);
		return deliveryMans;
	}
	
	public static List<DeliveryMan> findDeliveryManByVehicle(VEHICLES vehicle) {
		newInstance();
		List<DeliveryMan> deliveryMans;
		switch(vehicle) {
		case CAR:
			deliveryMans = dmo.findBy(DeliveryMan.VEHICLE, "Car");
			break;
		case MOTORBIKE:
			deliveryMans = dmo.findBy(DeliveryMan.VEHICLE, "Motorbike");
			break;
		case BIKE:
			deliveryMans = dmo.findBy(DeliveryMan.VEHICLE, "Bike");
			break;
		default: throw new IllegalArgumentException("Vehicle not supported: "+vehicle);
		}
		return deliveryMans;
	}
	
	public static DeliveryMan findDeliveryMan(String firstName, String lastName) {
		newInstance();
		if(!isValidStringName(firstName) || !isValidStringName(lastName))
			return null;
		List<DeliveryMan> deliveryMans = dmo.findBy(
				new String[]{DeliveryMan.FIRSTNAME, DeliveryMan.LASTNAME},
				new String[]{firstName, lastName});
		return (deliveryMans.size() > 0) ? deliveryMans.get(0) : null;
	}
	
	public static boolean isPersisted(DeliveryMan deliveryMan) {
		newInstance();
		if(!isValidStringName(deliveryMan.getFirstName()) || !isValidStringName(deliveryMan.getLastName()))
			return false;
		List<DeliveryMan> deliveryMans = dmo.findBy(
				new String[]{DeliveryMan.FIRSTNAME, DeliveryMan.LASTNAME},
				new String[]{deliveryMan.getFirstName(), deliveryMan.getLastName()});
		return (deliveryMans.size() > 0) ? true : false;
	}
	
	public static boolean updateDeliveryMan(DeliveryMan deliveryMan) {
		newInstance();
		if(findDeliveryMan(deliveryMan.getFirstName(), deliveryMan.getLastName()) == null)
			//Entity doesn't exist
			return false;
		DeliveryMan d = dmo.update(deliveryMan);
		return (d == null) ? false : true;
	}
	
	public static DeliveryMan updateDeliveryMan(DeliveryMan oldDeliveryMan, String newFirstName,String newLastName) {
		newInstance();
		if(findDeliveryMan(oldDeliveryMan.getFirstName(), oldDeliveryMan.getLastName()) == null)
			//Entity doesn't exist
			return null;
		DeliveryMan newDeliveryMan;
		switch(oldDeliveryMan.deliverWith()) {
		case "Bike":
			newDeliveryMan = new Rider(newFirstName,newLastName);
			break;
		case "Car":
			newDeliveryMan = new Driver(newFirstName,newLastName);
			break;
		case "Motorbike":
			newDeliveryMan = new Motorcyclist(newFirstName,newLastName);
			break;
		default:
			newDeliveryMan = null;
		
		}
		dmo.update(oldDeliveryMan, newDeliveryMan);
		return newDeliveryMan;
	}
	
	public static List<DeliveryMan> findAll() {
		newInstance();
		List<DeliveryMan> deliveryMans = dmo.getAll();
		return deliveryMans;
	}
	
	public static boolean deleteDeliveryMan(DeliveryMan deliveryMan) {
		newInstance();
		if(findDeliveryMan(deliveryMan.getFirstName(), deliveryMan.getLastName()) == null)
			//Entity doesn't exist
			return false;
		dmo.delete(deliveryMan);
		return true;
	}
	
	private static boolean isValidStringName(String name) {
		return (name == null || name.equals("")) ? false : true;
	}
	
	public static void close() {
		if(dmo != null)
			dmo.close();
		dmo=null;
		instance=null;
	}
	
}
