package services;

import java.util.ArrayList;
import java.util.List;
import exceptions.EntityWithSameNameAlreadyExistException;
import model.Customer;
import operations.CustomerOperations;

public class CustomerServices {
	
	private static CustomerServices instance;
	
	private static CustomerOperations co;
	
	private CustomerServices() {
		co = new CustomerOperations();
	}
	
	public static void newInstance() {
		if(instance == null)
			instance = new CustomerServices();
	}
	
	public static Customer createCustomer(String firstName, String lastName) throws IllegalArgumentException {
		newInstance();
		Customer c = new Customer(firstName, lastName);
		return !addCustomer(c) ? null : c;
	}
	
	private static boolean addCustomer(Customer customer) {
		newInstance();
		try {
			co.persist(customer);
		} catch (EntityWithSameNameAlreadyExistException e) {
			e.printStackTrace();
			return false;
		} 
		return true;
	}
	
	public static List<Customer> findCustomersByFirstName(String firstName) {
		newInstance();
		//Input control
		if(!isValidStringName(firstName))
			return new ArrayList<>();
		List<Customer> customers = co.findBy(Customer.FIRSTNAME, firstName);
		return customers;
	}
	
	public static List<Customer> findCustomersByLastName(String lastName) {
		newInstance();
		if(!isValidStringName(lastName))
			return new ArrayList<>();
		List<Customer> customers = co.findBy(Customer.LASTNAME, lastName);
		return customers;
	}
	
	public static Customer findCustomer(String firstName, String lastName) {
		newInstance();
		if(!isValidStringName(firstName) || !isValidStringName(lastName))
			return null;
		List<Customer> customers = co.findBy(
				new String[]{Customer.FIRSTNAME, Customer.LASTNAME},
				new String[]{firstName, lastName});
		return (customers.size() > 0) ? customers.get(0) : null;
	}
	
	public static boolean isPersisted(Customer customer) {
		newInstance();
		if(!isValidStringName(customer.getFirstName()) || !isValidStringName(customer.getLastName()))
			return false;
		List<Customer> customers = co.findBy(
				new String[]{Customer.FIRSTNAME, Customer.LASTNAME},
				new String[]{customer.getFirstName(), customer.getLastName()});
		return (customers.size() > 0) ? true : false;
	}
	
	public static List<Customer> findAll() {
		newInstance();
		List<Customer> customers = co.getAll();
		return customers;
	}
	
	
	public static Customer updateCustomer(Customer customer) {	
		newInstance();
		if(findCustomer(customer.getFirstName(), customer.getLastName()) == null)
			//Entity doesn't exist
			return null;
		Customer c = co.update(customer);
		return c;
	}
	
	public static Customer updateCustomer(Customer oldCustomer, String newFirstName, String newLastName) {
		newInstance();
		if(findCustomer(oldCustomer.getFirstName(), oldCustomer.getLastName()) == null)
			//Entity doesn't exist
			return null;
		Customer newCustomer = new Customer(newFirstName, newLastName);
		newCustomer.setIdCustomer(oldCustomer.getIdCustomer());
		co.update(oldCustomer, newCustomer);
		return newCustomer;
	}
	
	public static boolean deleteCustomer(Customer customer) {
		newInstance();
		if(findCustomer(customer.getFirstName(), customer.getLastName()) == null)
			//Entity doesn't exist
			return false;
		co.delete(customer);
		return true;
	}
	
	private static boolean isValidStringName(String name) {
		return (name == null || name.equals("")) ? false : true;
	}
	
	public static void close() {
		if(co != null)
			co.close();
		co=null;
		instance=null;
		
	}
	
}
