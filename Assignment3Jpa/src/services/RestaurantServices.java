package services;

import java.util.List;

import exceptions.EntityWithSameNameAlreadyExistException;
import model.Restaurant;
import operations.RestaurantOperations;

public class RestaurantServices {
	
	private static RestaurantServices instance;
	
	private static RestaurantOperations ro;
	
	private RestaurantServices() {
		ro = new RestaurantOperations();
	}
	
	public static void newInstance() {
		if(instance == null)
			instance = new RestaurantServices();
	}
	
	public static Restaurant createRestaurant(String name) throws IllegalArgumentException {
		newInstance();
		Restaurant r = new Restaurant(name);
		return !addRestaurant(r) ? null : r;
	}
	
	private static boolean addRestaurant(Restaurant restaurant) {
		newInstance();
		try {
			ro.persist(restaurant);
		} catch (EntityWithSameNameAlreadyExistException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static Restaurant findRestaurant(String name) {
		newInstance();
		//Input control
		if(!isValidStringName(name))
			return null;
		List<Restaurant> restaurants = ro.findBy(Restaurant.NAME, name);
		return (restaurants.size() > 0) ? restaurants.get(0) : null;
	}
	
	public static boolean isPersisted(Restaurant restaurant) {
		newInstance();
		//Input control
		if(!isValidStringName(restaurant.getName()))
			return false;
		List<Restaurant> restaurants = ro.findBy(Restaurant.NAME, restaurant.getName());
		return (restaurants.size() > 0) ? true : false;
	}
	
	public static boolean updateRestaurant(Restaurant restaurant) {
		newInstance();
		if(findRestaurant(restaurant.getName()) == null)
			//Entity doesn't exist
			return false;
		Restaurant r = ro.update(restaurant);
		return (r == null) ? false : true;
	}
	
	public static Restaurant updateRestaurant(Restaurant oldRestaurant, String newRestaurant) {
		newInstance();
		if(findRestaurant(oldRestaurant.getName()) == null)
			//Entity doesn't exist
			return null;
		Restaurant r = new Restaurant(newRestaurant);
		r.setIdRestaurant(oldRestaurant.getIdRestaurant());
		ro.update(oldRestaurant, r);
		return r;
	}
	
	public static boolean deleteRestaurant(Restaurant restaurant) {
		newInstance();
		if(findRestaurant(restaurant.getName()) == null)
			//Entity doesn't exist
			return false;
		ro.delete(restaurant);
		return true;
	}
	
	public static List<Restaurant> findAll() {
		newInstance();
		List<Restaurant> restaurants = ro.getAll();
		return restaurants;
	}
	
	private static boolean isValidStringName(String name) {
		return (name == null || name.equals("")) ? false : true;
	}
	
	public static void close() {
		if(ro != null)
			ro.close();
		ro=null;
		instance=null;
	}
}
