package services;

import java.util.ArrayList;
import java.util.List;

import exceptions.EntityWithSameNameAlreadyExistException;
import model.Product;
import operations.ProductOperations;

public class ProductServices {
	
	private static ProductServices instance;
	
	private static ProductOperations po;
	
	private ProductServices() {
		po = new ProductOperations();
	}
	
	public static void newInstance() {
		if(instance == null)
			instance = new ProductServices();
	}
	
	public static Product createProduct(String name) throws IllegalArgumentException {
		newInstance();
		Product p = new Product(name);
		return !addProduct(p) ? null : p;
	}
	
	private static boolean addProduct(Product product) {
		newInstance();
		try {
			po.persist(product);
		} catch (EntityWithSameNameAlreadyExistException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void addIngredientToProduct(Product product, Product ingredient, int quantity) {
		newInstance();
		if(findProduct(ingredient.getName()) == null)
			addProduct(ingredient);
		if(findProduct(product.getName()) == null)
			addProduct(product);
		product.addIngredient(ingredient, quantity);
		product = po.update(product);
	}
	
	public static boolean removeIngredientFromProduct(Product product, Product ingredient) {
		newInstance();
		if(findProduct(product.getName()) == null)
			return false;
		product.removeIngredient(ingredient);	
		product = po.update(product);
		return true;
	}
	
	public static Product findProduct(String name) {
		newInstance();
		//Input control
		if(!isValidStringName(name))
			return null;
		List<Product> products = po.findBy(Product.NAME, name);
		return (products.size() > 0) ? products.get(0) : null;
	}
	
	public static boolean isPersisted(Product product) {
		newInstance();
		//Input control
		if(!isValidStringName(product.getName()))
			return false;
		List<Product> products = po.findBy(Product.NAME, product.getName());
		return (products.size() > 0) ? true : false;
	}
	
	//Find the products which have as ingredient this specific ingredient
	public static List<Product> findProductsByIngredient(String ingredientName){
		newInstance();
		//Input control
		if(!isValidStringName(ingredientName))
			return new ArrayList<>();
		List<Product> products = po.findByIngredient(ingredientName);
		return products;
	}
	
	//Find the products which have ingredients in the given quantity
	public static List<Product> findProductsByQuantity(int quantity){
		newInstance();
		//Input control
		if(!isValidQuantity(quantity))
			return new ArrayList<>();
		List<Product> products = po.findBy(Product.QUANTITY, quantity);
		return products;
	}
	
	//Find the ingredients of the given principal product in the given quantity 
	public static List<Product> findIngredientsByQuantity(Product principalProduct, int quantity) {
		newInstance();
		//Input control
		if(!isValidQuantity(quantity))
			return new ArrayList<>();
		List<Product> products = po.findBy(
				new String[]{Product.NAME, Product.QUANTITY},
				new Object[]{principalProduct.getName(), quantity});
		return products;
	}
	
	//Find products which have as ingredient the given ingredient in the given quantity
	public static List<Product> findProductsByIngredientQuantity(String ingredientName, int quantity) {
		newInstance();
		//Input control
		if(!isValidStringName(ingredientName) || !isValidQuantity(quantity))
			return new ArrayList<>();
		List<Product> products = po.findByIngredient(ingredientName, quantity);
		return products;
	}
	
	public static boolean updateProduct(Product product) {
		newInstance();
		if(findProduct(product.getName()) == null)
			//Entity doesn't exist
			return false;
		Product p = po.update(product);
		return (p == null) ? false : true;
	}
	
	public static Product updateProduct(Product oldProduct, String name) {
		newInstance();
		if(findProduct(oldProduct.getName()) == null)
			//Entity doesn't exist
			return null;
		oldProduct.setName(name);
		Product newProduct = po.update(oldProduct);
		return newProduct;
	}
	
	public static boolean deleteProduct(Product product) {
		newInstance();
		if(findProduct(product.getName()) == null)
			//Entity doesn't exist
			return false;
		po.delete(product);
		return true;
	}
	
	public static List<Product> findAll() {
		newInstance();
		List<Product> products = po.getAll();
		return products;
	}
	
	public static void close() {
		if(po != null)
			po.close();
		po=null;
		instance=null;
	}
	
	
	private static boolean isValidStringName(String name) {
		return (name == null || name.equals("")) ? false : true;
	}
	
	private static boolean isValidQuantity(int quantity) {
		return (quantity > 0) ? true : false;
	}
}
