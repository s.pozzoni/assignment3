package services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import exceptions.EntityWithSameNameAlreadyExistException;
import model.Customer;
import model.DeliveryMan;
import model.FoodOrder;
import model.OrderDetail;
import model.Product;
import model.Restaurant;
import operations.OrderOperations;

public class OrderServices {
	
	private static OrderServices instance;
	
	private static OrderOperations oo;
	
	private OrderServices() {
		oo = new OrderOperations();
	}
	
	public static void newInstance() {
		if(instance == null)
			instance = new OrderServices();
	}
	
	public static FoodOrder addOrder(Customer customer, Restaurant restaurant, Product product, int quantity) {
		newInstance();
		FoodOrder order = new FoodOrder();
		order.setCustomer(customer);
		order.setRestaurant(restaurant);
		order.addProduct(product, quantity);
		try {
			oo.persist(order);
		} catch(EntityWithSameNameAlreadyExistException ex) {
			order = null;
		}
		return order;
	}
	
	public static FoodOrder addOrder(Customer customer, Restaurant restaurant, HashMap<Product, Integer> products) {
		newInstance();
		FoodOrder order = new FoodOrder();
		order.setCustomer(customer);
		order.setRestaurant(restaurant);
		for(Map.Entry<Product, Integer> entry : products.entrySet())
			order.addProduct(entry.getKey(), entry.getValue());
		try {
			oo.persist(order);
		} catch(EntityWithSameNameAlreadyExistException ex) {
			order = null;
		}
		return order;
	}
	
	public static FoodOrder addOrder(Customer customer, Restaurant restaurant, DeliveryMan deliveryMan, HashMap<Product, Integer> products) {
		newInstance();
		FoodOrder order = addOrder(customer, restaurant, products);
		order.setDeliveryMan(deliveryMan);
		order = oo.update(order);
		return order;
	}
	
	public static boolean addProductToOrder(Product product, int quantity, FoodOrder order) {
		newInstance();
		//Check if product is in db
		Product p = ProductServices.findProduct(product.getName());
		if(p == null)
			ProductServices.createProduct(product.getName());
		//Check if order already exists
		FoodOrder o = oo.get(order.getIdOrder());
		if(o == null) 
			return false;
		o.addProduct(p, quantity);
		order = oo.update(o);
		return true;
	}
	
	public static boolean removeProductFromOrder(Product product, FoodOrder order) {
		newInstance();
		//Check if product is in db
		Product p = ProductServices.findProduct(product.getName());
		if(p == null)
			return false;
		//Check if order already exists
		FoodOrder o = oo.get(order.getIdOrder());
		if(o == null) 
			return false;
		if(order.containProduct(p)) {
			OrderDetail od = order.removeProduct(p);
			List<OrderDetail> ods = new ArrayList<>(1);
			ods.add(od);
			OrderOperations.deleteOrderDetails(ods);
		}		
		return true;
	}
	
	public static boolean assignOrderToDeliveryMan(FoodOrder order, DeliveryMan deliveryMan) {
		newInstance();
		//Check if deliveryMan is in db
		DeliveryMan d = DeliveryManServices.findDeliveryMan(deliveryMan.getFirstName(), deliveryMan.getLastName());
		if(d == null)
			DeliveryManServices.createDeliveryMan(deliveryMan.getFirstName(), deliveryMan.getLastName(), deliveryMan.deliverWith());
		FoodOrder o = oo.get(order.getIdOrder());
		if(o == null)
			return false;
		order.setDeliveryMan(deliveryMan);
		oo.update(order);
		return true;
	}
	

	public static boolean deleteOrder(FoodOrder order) {
		newInstance();
		if(oo.get(order.getIdOrder()) == null)
			//Entity doesn't exist in db
			return false;
		oo.delete(order);
		return true;
	}
		
	public static List<FoodOrder> findOrders(Customer customer) {	
		newInstance();
		//Check if customer exists
		Customer c = CustomerServices.findCustomer(customer.getFirstName(), customer.getLastName());
		if(c == null)
			return new ArrayList<FoodOrder>(); 
		List<FoodOrder> orders = oo.findBy(FoodOrder.CUSTOMER, c.getIdCustomer());
		return orders;
	}
	
	public static List<FoodOrder> findOrders(Restaurant restaurant) {	
		newInstance();
		//Check if restaurant exists
		Restaurant r = RestaurantServices.findRestaurant(restaurant.getName());
		if(r == null)
			return new ArrayList<FoodOrder>(); 
		List<FoodOrder> orders = oo.findBy(FoodOrder.RESTAURANT, r.getIdRestaurant());
		return orders;
	}
	
	public static List<FoodOrder> findOrders(DeliveryMan deliveryMan) {	
		newInstance();
		//Check if deliveryMan exists
		DeliveryMan d = DeliveryManServices.findDeliveryMan(deliveryMan.getFirstName(), deliveryMan.getLastName());
		if(d == null)
			return new ArrayList<FoodOrder>(); 
		List<FoodOrder> orders = oo.findBy(FoodOrder.DELIVERYMAN, d.getIdDeliveryMan());
		return orders;
	}
	
	public static List<FoodOrder> findOrders(Product product) {	
		newInstance();
		//Check if product exists
		Product p = ProductServices.findProduct(product.getName());
		if(p == null)
			return new ArrayList<FoodOrder>(); 
		List<FoodOrder> orders = oo.findBy(Product.NAME, p.getName());
		return orders;
	}
	
	public List<FoodOrder> findOrders(Customer customer, Restaurant restaurant) {	
		newInstance();
		Customer c = CustomerServices.findCustomer(customer.getFirstName(), customer.getLastName());
		Restaurant r = RestaurantServices.findRestaurant(restaurant.getName());
		if(c == null || r == null)
			return new ArrayList<FoodOrder>(); 
		List<FoodOrder> orders = oo.findBy(
				new String[]{FoodOrder.CUSTOMER, FoodOrder.RESTAURANT},
				new Object[]{c.getIdCustomer(), r.getIdRestaurant()});
		return orders;
	}
	
	public List<FoodOrder> findOrders(Restaurant restaurant, DeliveryMan deliveryMan) {	
		newInstance();
		Restaurant r = RestaurantServices.findRestaurant(restaurant.getName());
		DeliveryMan d = DeliveryManServices.findDeliveryMan(deliveryMan.getFirstName(), deliveryMan.getLastName());
		if(r == null || d == null)
			return new ArrayList<FoodOrder>(); 
		List<FoodOrder> orders = oo.findBy(
				new String[]{FoodOrder.RESTAURANT, FoodOrder.DELIVERYMAN},
				new Object[]{r.getIdRestaurant(), d.getIdDeliveryMan()});
		return orders;
	}
	
	public static List<FoodOrder> findOrders(Customer customer, Restaurant restaurant, DeliveryMan deliveryMan) {	
		newInstance();
		Customer c = CustomerServices.findCustomer(customer.getFirstName(), customer.getLastName());
		Restaurant r = RestaurantServices.findRestaurant(restaurant.getName());
		DeliveryMan d = DeliveryManServices.findDeliveryMan(deliveryMan.getFirstName(), deliveryMan.getLastName());
		if(c == null || r == null || d == null)
			return new ArrayList<FoodOrder>();
		List<FoodOrder> orders = oo.findBy(
				new String[]{FoodOrder.CUSTOMER, FoodOrder.RESTAURANT, FoodOrder.DELIVERYMAN},
				new Object[]{c.getIdCustomer(), r.getIdRestaurant(), d.getIdDeliveryMan()});
		return orders;
	}
	
	public static List<FoodOrder> findOrders(Customer customer, Restaurant restaurant, Product product) {	
		newInstance();

		Customer c = CustomerServices.findCustomer(customer.getFirstName(), customer.getLastName());
		Restaurant r = RestaurantServices.findRestaurant(restaurant.getName());
		Product p = ProductServices.findProduct(product.getName());
		if(c == null || r == null || p == null)
			return new ArrayList<FoodOrder>();
		List<FoodOrder> orders = oo.findBy(
				new String[]{FoodOrder.CUSTOMER, FoodOrder.RESTAURANT, Product.NAME},
				new Object[]{c.getIdCustomer(), r.getIdRestaurant(), p.getName()});
		return orders;
	}
	

	public static FoodOrder findOrder(Long idFoodOrder) {	
		newInstance();
		if(idFoodOrder==null)
			return null;
		List<FoodOrder> orders = oo.findBy(FoodOrder.IDFOODORDER, idFoodOrder);
		return orders != null && orders.size()>0 ? orders.get(0) : null;
	}
	
	
	public static List<FoodOrder> findOrders(Customer customer, Restaurant restaurant, Product product, int quantity) {	
		newInstance();
		Customer c = CustomerServices.findCustomer(customer.getFirstName(), customer.getLastName());
		Restaurant r = RestaurantServices.findRestaurant(restaurant.getName());
		Product p = ProductServices.findProduct(product.getName());
		if(c == null || r == null || p == null)
			return new ArrayList<FoodOrder>(); //or null?
		List<FoodOrder> orders = oo.findBy(
				new String[]{FoodOrder.CUSTOMER, FoodOrder.RESTAURANT, Product.NAME, Product.QUANTITY},
				new Object[]{c.getIdCustomer(), r.getIdRestaurant(), p.getName(), quantity});
		return orders;
	}
	
	public static List<FoodOrder> findAll() {
		newInstance();
		List<FoodOrder> orders = oo.getAll();
		return orders;
	}
	public static void close() {
		if(oo != null)
			oo.close();
		oo = null;
		instance = null;
	}
	
}
